package com.broadwayharvey.watson.repository.integration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.broadwayharvey.watson.model.User;
import com.broadwayharvey.watson.repository.UserRepository;

public class UserRepositoryTest extends BaseRepositoryIntegrationTest {

  @Autowired
  private UserRepository userRepository;

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  @Test
  public void testSaveUser() {
    User user = new User("Francis", "blah");
    userRepository.save(user);
  }

  @Test
  public void testFindByUsername() {
    User user = (User) userRepository.findByUsername("Francis");
    Assert.notNull(user);
  }

}
