package com.broadwayharvey.watson.repository.integration;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.broadwayharvey.watson.model.SystemInformation;
import com.broadwayharvey.watson.repository.SystemInformationRepository;
import com.google.common.collect.Lists;

public class SystemInformationRepositoryTest extends BaseRepositoryIntegrationTest {

  @Autowired
  private SystemInformationRepository systemInformationRepository;

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  @Test
  public void testSaveSystemInformation() {
    SystemInformation systemInformation = new SystemInformation();
    systemInformation.setVerificationAPIPassword("CarinvalCruise.1234#");
    systemInformation.setVerificationAPIUsername("CarinvalCruise");
    systemInformation.setVerificationAPIURI("http://192.168.58.35:8089/Fls_Customer_Test/fls.asmx");
    systemInformationRepository.save(systemInformation);
  }

  @Test
  public void testFindAllSystemInformation() {
    List<SystemInformation> systemInformationList =
        Lists.newArrayList(systemInformationRepository.findAll());
    for (SystemInformation s : systemInformationList) {
      logger.info(s);
    }
  }
  // @Test
  // public void testFindBySystemInformationname() {
  // SystemInformation systemInformation = (SystemInformation) systemInformationRepository.g;
  // Assert.notNull(systemInformation);
  // }

}
