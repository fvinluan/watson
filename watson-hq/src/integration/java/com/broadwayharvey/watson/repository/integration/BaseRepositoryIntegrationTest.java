package com.broadwayharvey.watson.repository.integration;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Base class for running Dao tests.
 * 
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/applicationContext-mongo.xml"})
@Ignore
public class BaseRepositoryIntegrationTest extends AbstractJUnit4SpringContextTests {

}
