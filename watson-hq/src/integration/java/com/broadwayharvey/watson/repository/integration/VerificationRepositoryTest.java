package com.broadwayharvey.watson.repository.integration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.broadwayharvey.watson.model.Verification;
import com.broadwayharvey.watson.repository.VerificationRepository;

public class VerificationRepositoryTest extends BaseRepositoryIntegrationTest {

  @Autowired
  private VerificationRepository verificationRepository;

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  @Test
  public void testFindByDocumentNumberAndDocumentTypeAndIssuingCountry() {
    Verification verification =
        verificationRepository.findByDocumentNumberAndDocumentTypeAndIssuingCountry("1234", "1",
            "USA");
    logger.info(verification.toString());
    assertNotNull(verification);
  }

  @Test
  public void testSave() {
    Verification verification = new Verification("1234", "1", "USA", "1234");
    verificationRepository.save(verification);
  }

  public void setVerificationRepository(VerificationRepository verificationRepository) {
    this.verificationRepository = verificationRepository;
  }

  @Test
  public void testFindByIssuingCountry() {
    List<Verification> verifications = verificationRepository.findByIssuingCountry("USA");
    for (Verification v : verifications) {
      logger.info(v.toString());
    }
    assertTrue(CollectionUtils.isNotEmpty(verifications));
    assertNotNull(verifications);

  }

  @Test
  public void testFindByDocumentType() {
    List<Verification> verifications = verificationRepository.findByDocumentType("1");
    for (Verification v : verifications) {
      logger.info(v.toString());
    }
    assertTrue(CollectionUtils.isNotEmpty(verifications));
    assertNotNull(verifications);

  }
}
