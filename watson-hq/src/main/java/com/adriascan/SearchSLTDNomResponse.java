
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchSLTDNomResult" type="{http://tempuri.org/}SLTDNomSearchResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchSLTDNomResult"
})
@XmlRootElement(name = "SearchSLTDNomResponse", namespace = "http://tempuri.org/")
public class SearchSLTDNomResponse {

    @XmlElement(name = "SearchSLTDNomResult", namespace = "http://tempuri.org/")
    protected SLTDNomSearchResult searchSLTDNomResult;

    /**
     * Gets the value of the searchSLTDNomResult property.
     * 
     * @return
     *     possible object is
     *     {@link SLTDNomSearchResult }
     *     
     */
    public SLTDNomSearchResult getSearchSLTDNomResult() {
        return searchSLTDNomResult;
    }

    /**
     * Sets the value of the searchSLTDNomResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SLTDNomSearchResult }
     *     
     */
    public void setSearchSLTDNomResult(SLTDNomSearchResult value) {
        this.searchSLTDNomResult = value;
    }

}
