
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchDocumentResult" type="{http://tempuri.org/}SearchDocumentResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchDocumentResult"
})
@XmlRootElement(name = "SearchDocumentResponse", namespace = "http://tempuri.org/")
public class SearchDocumentResponse {

    @XmlElement(name = "SearchDocumentResult", namespace = "http://tempuri.org/")
    protected SearchDocumentResult searchDocumentResult;

    /**
     * Gets the value of the searchDocumentResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDocumentResult }
     *     
     */
    public SearchDocumentResult getSearchDocumentResult() {
        return searchDocumentResult;
    }

    /**
     * Sets the value of the searchDocumentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDocumentResult }
     *     
     */
    public void setSearchDocumentResult(SearchDocumentResult value) {
        this.searchDocumentResult = value;
    }

}
