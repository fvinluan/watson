
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="application" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiveEmails" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="receivePositiveNotifications" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="receiveConnectionNotifications" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="receiveErrorNotifications" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "username",
    "password",
    "application",
    "id",
    "email",
    "receiveEmails",
    "receivePositiveNotifications",
    "receiveConnectionNotifications",
    "receiveErrorNotifications"
})
@XmlRootElement(name = "SaveNotificationRecipient", namespace = "http://tempuri.org/")
public class SaveNotificationRecipient {

    @XmlElement(namespace = "http://tempuri.org/")
    protected String username;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String password;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String application;
    @XmlElement(name = "ID", namespace = "http://tempuri.org/")
    protected int id;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String email;
    @XmlElement(namespace = "http://tempuri.org/")
    protected boolean receiveEmails;
    @XmlElement(namespace = "http://tempuri.org/")
    protected boolean receivePositiveNotifications;
    @XmlElement(namespace = "http://tempuri.org/")
    protected boolean receiveConnectionNotifications;
    @XmlElement(namespace = "http://tempuri.org/")
    protected boolean receiveErrorNotifications;

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the application property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplication() {
        return application;
    }

    /**
     * Sets the value of the application property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplication(String value) {
        this.application = value;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the receiveEmails property.
     * 
     */
    public boolean isReceiveEmails() {
        return receiveEmails;
    }

    /**
     * Sets the value of the receiveEmails property.
     * 
     */
    public void setReceiveEmails(boolean value) {
        this.receiveEmails = value;
    }

    /**
     * Gets the value of the receivePositiveNotifications property.
     * 
     */
    public boolean isReceivePositiveNotifications() {
        return receivePositiveNotifications;
    }

    /**
     * Sets the value of the receivePositiveNotifications property.
     * 
     */
    public void setReceivePositiveNotifications(boolean value) {
        this.receivePositiveNotifications = value;
    }

    /**
     * Gets the value of the receiveConnectionNotifications property.
     * 
     */
    public boolean isReceiveConnectionNotifications() {
        return receiveConnectionNotifications;
    }

    /**
     * Sets the value of the receiveConnectionNotifications property.
     * 
     */
    public void setReceiveConnectionNotifications(boolean value) {
        this.receiveConnectionNotifications = value;
    }

    /**
     * Gets the value of the receiveErrorNotifications property.
     * 
     */
    public boolean isReceiveErrorNotifications() {
        return receiveErrorNotifications;
    }

    /**
     * Sets the value of the receiveErrorNotifications property.
     * 
     */
    public void setReceiveErrorNotifications(boolean value) {
        this.receiveErrorNotifications = value;
    }

}
