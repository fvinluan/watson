
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="checkFLSConnection" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkFLSConnection"
})
@XmlRootElement(name = "Ping", namespace = "http://tempuri.org/")
public class Ping {

    @XmlElement(namespace = "http://tempuri.org/")
    protected boolean checkFLSConnection;

    /**
     * Gets the value of the checkFLSConnection property.
     * 
     */
    public boolean isCheckFLSConnection() {
        return checkFLSConnection;
    }

    /**
     * Sets the value of the checkFLSConnection property.
     * 
     */
    public void setCheckFLSConnection(boolean value) {
        this.checkFLSConnection = value;
    }

}
