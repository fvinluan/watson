
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchRestrictionToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchRestrictionToken">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CountryID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="RestrictionType" type="{http://tempuri.org/}SearchRestrictionType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchRestrictionToken", namespace = "http://tempuri.org/", propOrder = {
    "userID",
    "countryID",
    "restrictionType"
})
public class SearchRestrictionToken {

    @XmlElement(name = "UserID", namespace = "http://tempuri.org/")
    protected long userID;
    @XmlElement(name = "CountryID", namespace = "http://tempuri.org/")
    protected long countryID;
    @XmlElement(name = "RestrictionType", namespace = "http://tempuri.org/", required = true)
    protected SearchRestrictionType restrictionType;

    /**
     * Gets the value of the userID property.
     * 
     */
    public long getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     */
    public void setUserID(long value) {
        this.userID = value;
    }

    /**
     * Gets the value of the countryID property.
     * 
     */
    public long getCountryID() {
        return countryID;
    }

    /**
     * Sets the value of the countryID property.
     * 
     */
    public void setCountryID(long value) {
        this.countryID = value;
    }

    /**
     * Gets the value of the restrictionType property.
     * 
     * @return
     *     possible object is
     *     {@link SearchRestrictionType }
     *     
     */
    public SearchRestrictionType getRestrictionType() {
        return restrictionType;
    }

    /**
     * Sets the value of the restrictionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchRestrictionType }
     *     
     */
    public void setRestrictionType(SearchRestrictionType value) {
        this.restrictionType = value;
    }

}
