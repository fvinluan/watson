
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSigningCertsResult" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSigningCertsResult"
})
@XmlRootElement(name = "GetSigningCertsResponse", namespace = "http://tempuri.org/")
public class GetSigningCertsResponse {

    @XmlElement(name = "GetSigningCertsResult", namespace = "http://tempuri.org/")
    protected ArrayOfString getSigningCertsResult;

    /**
     * Gets the value of the getSigningCertsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getGetSigningCertsResult() {
        return getSigningCertsResult;
    }

    /**
     * Sets the value of the getSigningCertsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setGetSigningCertsResult(ArrayOfString value) {
        this.getSigningCertsResult = value;
    }

}
