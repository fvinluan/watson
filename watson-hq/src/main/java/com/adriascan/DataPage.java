
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataPage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataPage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PageNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="IDNumbers" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartRowNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataPage", namespace = "http://tempuri.org/", propOrder = {
    "pageNumber",
    "idNumbers",
    "startRowNumber"
})
public class DataPage {

    @XmlElement(name = "PageNumber", namespace = "http://tempuri.org/")
    protected long pageNumber;
    @XmlElement(name = "IDNumbers", namespace = "http://tempuri.org/")
    protected String idNumbers;
    @XmlElement(name = "StartRowNumber", namespace = "http://tempuri.org/")
    protected int startRowNumber;

    /**
     * Gets the value of the pageNumber property.
     * 
     */
    public long getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     */
    public void setPageNumber(long value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the idNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDNumbers() {
        return idNumbers;
    }

    /**
     * Sets the value of the idNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDNumbers(String value) {
        this.idNumbers = value;
    }

    /**
     * Gets the value of the startRowNumber property.
     * 
     */
    public int getStartRowNumber() {
        return startRowNumber;
    }

    /**
     * Sets the value of the startRowNumber property.
     * 
     */
    public void setStartRowNumber(int value) {
        this.startRowNumber = value;
    }

}
