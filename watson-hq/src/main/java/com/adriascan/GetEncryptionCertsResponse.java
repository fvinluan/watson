
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetEncryptionCertsResult" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEncryptionCertsResult"
})
@XmlRootElement(name = "GetEncryptionCertsResponse", namespace = "http://tempuri.org/")
public class GetEncryptionCertsResponse {

    @XmlElement(name = "GetEncryptionCertsResult", namespace = "http://tempuri.org/")
    protected ArrayOfString getEncryptionCertsResult;

    /**
     * Gets the value of the getEncryptionCertsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getGetEncryptionCertsResult() {
        return getEncryptionCertsResult;
    }

    /**
     * Sets the value of the getEncryptionCertsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setGetEncryptionCertsResult(ArrayOfString value) {
        this.getEncryptionCertsResult = value;
    }

}
