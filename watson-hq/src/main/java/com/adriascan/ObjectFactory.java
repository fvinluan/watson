
package com.adriascan;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.adriascan package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.adriascan
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSearchLogTagsResponse }
     * 
     */
    public GetSearchLogTagsResponse createGetSearchLogTagsResponse() {
        return new GetSearchLogTagsResponse();
    }

    /**
     * Create an instance of {@link GetSettingsResponse }
     * 
     */
    public GetSettingsResponse createGetSettingsResponse() {
        return new GetSettingsResponse();
    }

    /**
     * Create an instance of {@link GetNotificationRecipientResponse }
     * 
     */
    public GetNotificationRecipientResponse createGetNotificationRecipientResponse() {
        return new GetNotificationRecipientResponse();
    }

    /**
     * Create an instance of {@link SaveSettings }
     * 
     */
    public SaveSettings createSaveSettings() {
        return new SaveSettings();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link GetRolesResponse }
     * 
     */
    public GetRolesResponse createGetRolesResponse() {
        return new GetRolesResponse();
    }

    /**
     * Create an instance of {@link GetUsersResponse }
     * 
     */
    public GetUsersResponse createGetUsersResponse() {
        return new GetUsersResponse();
    }

    /**
     * Create an instance of {@link GetNotificationRecipientsResponse }
     * 
     */
    public GetNotificationRecipientsResponse createGetNotificationRecipientsResponse() {
        return new GetNotificationRecipientsResponse();
    }

    /**
     * Create an instance of {@link GetSpecificPageResponse }
     * 
     */
    public GetSpecificPageResponse createGetSpecificPageResponse() {
        return new GetSpecificPageResponse();
    }

    /**
     * Create an instance of {@link GetCountriesResponse }
     * 
     */
    public GetCountriesResponse createGetCountriesResponse() {
        return new GetCountriesResponse();
    }

    /**
     * Create an instance of {@link GetSearchLogResponse }
     * 
     */
    public GetSearchLogResponse createGetSearchLogResponse() {
        return new GetSearchLogResponse();
    }

    /**
     * Create an instance of {@link PagedDataTable }
     * 
     */
    public PagedDataTable createPagedDataTable() {
        return new PagedDataTable();
    }

    /**
     * Create an instance of {@link GetSearchLogPagedResponse }
     * 
     */
    public GetSearchLogPagedResponse createGetSearchLogPagedResponse() {
        return new GetSearchLogPagedResponse();
    }

    /**
     * Create an instance of {@link GetUser }
     * 
     */
    public GetUser createGetUser() {
        return new GetUser();
    }

    /**
     * Create an instance of {@link Search }
     * 
     */
    public Search createSearch() {
        return new Search();
    }

    /**
     * Create an instance of {@link PatchDatabaseResponse }
     * 
     */
    public PatchDatabaseResponse createPatchDatabaseResponse() {
        return new PatchDatabaseResponse();
    }

    /**
     * Create an instance of {@link CanSearchSLTDAndNominals }
     * 
     */
    public CanSearchSLTDAndNominals createCanSearchSLTDAndNominals() {
        return new CanSearchSLTDAndNominals();
    }

    /**
     * Create an instance of {@link TestSearchNominals }
     * 
     */
    public TestSearchNominals createTestSearchNominals() {
        return new TestSearchNominals();
    }

    /**
     * Create an instance of {@link TestSearch }
     * 
     */
    public TestSearch createTestSearch() {
        return new TestSearch();
    }

    /**
     * Create an instance of {@link GetSearchLogTagsResponse.GetSearchLogTagsResult }
     * 
     */
    public GetSearchLogTagsResponse.GetSearchLogTagsResult createGetSearchLogTagsResponseGetSearchLogTagsResult() {
        return new GetSearchLogTagsResponse.GetSearchLogTagsResult();
    }

    /**
     * Create an instance of {@link AdminExists }
     * 
     */
    public AdminExists createAdminExists() {
        return new AdminExists();
    }

    /**
     * Create an instance of {@link GetSearchRestrictionsResponse }
     * 
     */
    public GetSearchRestrictionsResponse createGetSearchRestrictionsResponse() {
        return new GetSearchRestrictionsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSearchRestrictionToken }
     * 
     */
    public ArrayOfSearchRestrictionToken createArrayOfSearchRestrictionToken() {
        return new ArrayOfSearchRestrictionToken();
    }

    /**
     * Create an instance of {@link GetSettingsResponse.GetSettingsResult }
     * 
     */
    public GetSettingsResponse.GetSettingsResult createGetSettingsResponseGetSettingsResult() {
        return new GetSettingsResponse.GetSettingsResult();
    }

    /**
     * Create an instance of {@link GetNotificationRecipientResponse.GetNotificationRecipientResult }
     * 
     */
    public GetNotificationRecipientResponse.GetNotificationRecipientResult createGetNotificationRecipientResponseGetNotificationRecipientResult() {
        return new GetNotificationRecipientResponse.GetNotificationRecipientResult();
    }

    /**
     * Create an instance of {@link SaveSettings.SettingsTable }
     * 
     */
    public SaveSettings.SettingsTable createSaveSettingsSettingsTable() {
        return new SaveSettings.SettingsTable();
    }

    /**
     * Create an instance of {@link GetSigningCerts }
     * 
     */
    public GetSigningCerts createGetSigningCerts() {
        return new GetSigningCerts();
    }

    /**
     * Create an instance of {@link AuthenticateUser }
     * 
     */
    public AuthenticateUser createAuthenticateUser() {
        return new AuthenticateUser();
    }

    /**
     * Create an instance of {@link DeleteSystemLog }
     * 
     */
    public DeleteSystemLog createDeleteSystemLog() {
        return new DeleteSystemLog();
    }

    /**
     * Create an instance of {@link ArrayOfLong }
     * 
     */
    public ArrayOfLong createArrayOfLong() {
        return new ArrayOfLong();
    }

    /**
     * Create an instance of {@link GetSearchLogTags }
     * 
     */
    public GetSearchLogTags createGetSearchLogTags() {
        return new GetSearchLogTags();
    }

    /**
     * Create an instance of {@link GetEncryptionCerts }
     * 
     */
    public GetEncryptionCerts createGetEncryptionCerts() {
        return new GetEncryptionCerts();
    }

    /**
     * Create an instance of {@link GetUniqueSearchTagNames }
     * 
     */
    public GetUniqueSearchTagNames createGetUniqueSearchTagNames() {
        return new GetUniqueSearchTagNames();
    }

    /**
     * Create an instance of {@link SearchDocumentResponse }
     * 
     */
    public SearchDocumentResponse createSearchDocumentResponse() {
        return new SearchDocumentResponse();
    }

    /**
     * Create an instance of {@link SearchDocumentResult }
     * 
     */
    public SearchDocumentResult createSearchDocumentResult() {
        return new SearchDocumentResult();
    }

    /**
     * Create an instance of {@link SetSettingResponse }
     * 
     */
    public SetSettingResponse createSetSettingResponse() {
        return new SetSettingResponse();
    }

    /**
     * Create an instance of {@link SaveUserResponse }
     * 
     */
    public SaveUserResponse createSaveUserResponse() {
        return new SaveUserResponse();
    }

    /**
     * Create an instance of {@link GetVersion }
     * 
     */
    public GetVersion createGetVersion() {
        return new GetVersion();
    }

    /**
     * Create an instance of {@link GetUniqueNumberResponse }
     * 
     */
    public GetUniqueNumberResponse createGetUniqueNumberResponse() {
        return new GetUniqueNumberResponse();
    }

    /**
     * Create an instance of {@link SetSetting }
     * 
     */
    public SetSetting createSetSetting() {
        return new SetSetting();
    }

    /**
     * Create an instance of {@link Stop }
     * 
     */
    public Stop createStop() {
        return new Stop();
    }

    /**
     * Create an instance of {@link SaveNotificationRecipientResponse }
     * 
     */
    public SaveNotificationRecipientResponse createSaveNotificationRecipientResponse() {
        return new SaveNotificationRecipientResponse();
    }

    /**
     * Create an instance of {@link AdminExistsResponse }
     * 
     */
    public AdminExistsResponse createAdminExistsResponse() {
        return new AdminExistsResponse();
    }

    /**
     * Create an instance of {@link SaveUser }
     * 
     */
    public SaveUser createSaveUser() {
        return new SaveUser();
    }

    /**
     * Create an instance of {@link ApplicationUserToken }
     * 
     */
    public ApplicationUserToken createApplicationUserToken() {
        return new ApplicationUserToken();
    }

    /**
     * Create an instance of {@link Ping }
     * 
     */
    public Ping createPing() {
        return new Ping();
    }

    /**
     * Create an instance of {@link GetSystemLogPagedResponse }
     * 
     */
    public GetSystemLogPagedResponse createGetSystemLogPagedResponse() {
        return new GetSystemLogPagedResponse();
    }

    /**
     * Create an instance of {@link SearchSLTDNomResponse }
     * 
     */
    public SearchSLTDNomResponse createSearchSLTDNomResponse() {
        return new SearchSLTDNomResponse();
    }

    /**
     * Create an instance of {@link SLTDNomSearchResult }
     * 
     */
    public SLTDNomSearchResult createSLTDNomSearchResult() {
        return new SLTDNomSearchResult();
    }

    /**
     * Create an instance of {@link DeleteSearchHistory }
     * 
     */
    public DeleteSearchHistory createDeleteSearchHistory() {
        return new DeleteSearchHistory();
    }

    /**
     * Create an instance of {@link GetSearchRestrictions }
     * 
     */
    public GetSearchRestrictions createGetSearchRestrictions() {
        return new GetSearchRestrictions();
    }

    /**
     * Create an instance of {@link SaveSettingsResponse }
     * 
     */
    public SaveSettingsResponse createSaveSettingsResponse() {
        return new SaveSettingsResponse();
    }

    /**
     * Create an instance of {@link GetUserResponse.GetUserResult }
     * 
     */
    public GetUserResponse.GetUserResult createGetUserResponseGetUserResult() {
        return new GetUserResponse.GetUserResult();
    }

    /**
     * Create an instance of {@link GetSettings }
     * 
     */
    public GetSettings createGetSettings() {
        return new GetSettings();
    }

    /**
     * Create an instance of {@link TestSearchNominalsResponse }
     * 
     */
    public TestSearchNominalsResponse createTestSearchNominalsResponse() {
        return new TestSearchNominalsResponse();
    }

    /**
     * Create an instance of {@link SearchDetails }
     * 
     */
    public SearchDetails createSearchDetails() {
        return new SearchDetails();
    }

    /**
     * Create an instance of {@link GetUniqueNumber }
     * 
     */
    public GetUniqueNumber createGetUniqueNumber() {
        return new GetUniqueNumber();
    }

    /**
     * Create an instance of {@link CreateNotificationRecipientResponse }
     * 
     */
    public CreateNotificationRecipientResponse createCreateNotificationRecipientResponse() {
        return new CreateNotificationRecipientResponse();
    }

    /**
     * Create an instance of {@link CreateFirstAdminResponse }
     * 
     */
    public CreateFirstAdminResponse createCreateFirstAdminResponse() {
        return new CreateFirstAdminResponse();
    }

    /**
     * Create an instance of {@link ActivateLicenseKey }
     * 
     */
    public ActivateLicenseKey createActivateLicenseKey() {
        return new ActivateLicenseKey();
    }

    /**
     * Create an instance of {@link CanSearchSLTDAndNominalsResponse }
     * 
     */
    public CanSearchSLTDAndNominalsResponse createCanSearchSLTDAndNominalsResponse() {
        return new CanSearchSLTDAndNominalsResponse();
    }

    /**
     * Create an instance of {@link GetCountries }
     * 
     */
    public GetCountries createGetCountries() {
        return new GetCountries();
    }

    /**
     * Create an instance of {@link PingOtherServiceResponse }
     * 
     */
    public PingOtherServiceResponse createPingOtherServiceResponse() {
        return new PingOtherServiceResponse();
    }

    /**
     * Create an instance of {@link GetStatusResponse }
     * 
     */
    public GetStatusResponse createGetStatusResponse() {
        return new GetStatusResponse();
    }

    /**
     * Create an instance of {@link ServiceStatus }
     * 
     */
    public ServiceStatus createServiceStatus() {
        return new ServiceStatus();
    }

    /**
     * Create an instance of {@link GetRolesResponse.GetRolesResult }
     * 
     */
    public GetRolesResponse.GetRolesResult createGetRolesResponseGetRolesResult() {
        return new GetRolesResponse.GetRolesResult();
    }

    /**
     * Create an instance of {@link GetNotificationRecipients }
     * 
     */
    public GetNotificationRecipients createGetNotificationRecipients() {
        return new GetNotificationRecipients();
    }

    /**
     * Create an instance of {@link GetSpecificPage }
     * 
     */
    public GetSpecificPage createGetSpecificPage() {
        return new GetSpecificPage();
    }

    /**
     * Create an instance of {@link DataPage }
     * 
     */
    public DataPage createDataPage() {
        return new DataPage();
    }

    /**
     * Create an instance of {@link PatchDatabase }
     * 
     */
    public PatchDatabase createPatchDatabase() {
        return new PatchDatabase();
    }

    /**
     * Create an instance of {@link GetUsersResponse.GetUsersResult }
     * 
     */
    public GetUsersResponse.GetUsersResult createGetUsersResponseGetUsersResult() {
        return new GetUsersResponse.GetUsersResult();
    }

    /**
     * Create an instance of {@link DeleteNotificationRecipient }
     * 
     */
    public DeleteNotificationRecipient createDeleteNotificationRecipient() {
        return new DeleteNotificationRecipient();
    }

    /**
     * Create an instance of {@link SearchSLTDNom }
     * 
     */
    public SearchSLTDNom createSearchSLTDNom() {
        return new SearchSLTDNom();
    }

    /**
     * Create an instance of {@link GetNotificationRecipient }
     * 
     */
    public GetNotificationRecipient createGetNotificationRecipient() {
        return new GetNotificationRecipient();
    }

    /**
     * Create an instance of {@link GetNotificationRecipientsResponse.GetNotificationRecipientsResult }
     * 
     */
    public GetNotificationRecipientsResponse.GetNotificationRecipientsResult createGetNotificationRecipientsResponseGetNotificationRecipientsResult() {
        return new GetNotificationRecipientsResponse.GetNotificationRecipientsResult();
    }

    /**
     * Create an instance of {@link GetSpecificPageResponse.GetSpecificPageResult }
     * 
     */
    public GetSpecificPageResponse.GetSpecificPageResult createGetSpecificPageResponseGetSpecificPageResult() {
        return new GetSpecificPageResponse.GetSpecificPageResult();
    }

    /**
     * Create an instance of {@link GetCompanyInfoResponse }
     * 
     */
    public GetCompanyInfoResponse createGetCompanyInfoResponse() {
        return new GetCompanyInfoResponse();
    }

    /**
     * Create an instance of {@link CompanyInfo }
     * 
     */
    public CompanyInfo createCompanyInfo() {
        return new CompanyInfo();
    }

    /**
     * Create an instance of {@link DeleteNotificationRecipientResponse }
     * 
     */
    public DeleteNotificationRecipientResponse createDeleteNotificationRecipientResponse() {
        return new DeleteNotificationRecipientResponse();
    }

    /**
     * Create an instance of {@link SearchDocument }
     * 
     */
    public SearchDocument createSearchDocument() {
        return new SearchDocument();
    }

    /**
     * Create an instance of {@link ArrayOfSearchTag }
     * 
     */
    public ArrayOfSearchTag createArrayOfSearchTag() {
        return new ArrayOfSearchTag();
    }

    /**
     * Create an instance of {@link CreateNotificationRecipient }
     * 
     */
    public CreateNotificationRecipient createCreateNotificationRecipient() {
        return new CreateNotificationRecipient();
    }

    /**
     * Create an instance of {@link GetInterfaceVersion }
     * 
     */
    public GetInterfaceVersion createGetInterfaceVersion() {
        return new GetInterfaceVersion();
    }

    /**
     * Create an instance of {@link PingOtherService }
     * 
     */
    public PingOtherService createPingOtherService() {
        return new PingOtherService();
    }

    /**
     * Create an instance of {@link DeleteSearchHistoryResponse }
     * 
     */
    public DeleteSearchHistoryResponse createDeleteSearchHistoryResponse() {
        return new DeleteSearchHistoryResponse();
    }

    /**
     * Create an instance of {@link GetSearchLog }
     * 
     */
    public GetSearchLog createGetSearchLog() {
        return new GetSearchLog();
    }

    /**
     * Create an instance of {@link SearchLogToken }
     * 
     */
    public SearchLogToken createSearchLogToken() {
        return new SearchLogToken();
    }

    /**
     * Create an instance of {@link GetPagedDataResponse }
     * 
     */
    public GetPagedDataResponse createGetPagedDataResponse() {
        return new GetPagedDataResponse();
    }

    /**
     * Create an instance of {@link GetCountriesResponse.GetCountriesResult }
     * 
     */
    public GetCountriesResponse.GetCountriesResult createGetCountriesResponseGetCountriesResult() {
        return new GetCountriesResponse.GetCountriesResult();
    }

    /**
     * Create an instance of {@link GetRoles }
     * 
     */
    public GetRoles createGetRoles() {
        return new GetRoles();
    }

    /**
     * Create an instance of {@link GetEncryptionCertsResponse }
     * 
     */
    public GetEncryptionCertsResponse createGetEncryptionCertsResponse() {
        return new GetEncryptionCertsResponse();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link Start }
     * 
     */
    public Start createStart() {
        return new Start();
    }

    /**
     * Create an instance of {@link TestSearchResponse }
     * 
     */
    public TestSearchResponse createTestSearchResponse() {
        return new TestSearchResponse();
    }

    /**
     * Create an instance of {@link SearchNominalsResponse }
     * 
     */
    public SearchNominalsResponse createSearchNominalsResponse() {
        return new SearchNominalsResponse();
    }

    /**
     * Create an instance of {@link GetSettingResponse }
     * 
     */
    public GetSettingResponse createGetSettingResponse() {
        return new GetSettingResponse();
    }

    /**
     * Create an instance of {@link GetGuid }
     * 
     */
    public GetGuid createGetGuid() {
        return new GetGuid();
    }

    /**
     * Create an instance of {@link SearchNominals }
     * 
     */
    public SearchNominals createSearchNominals() {
        return new SearchNominals();
    }

    /**
     * Create an instance of {@link AuthenticateUserResponse }
     * 
     */
    public AuthenticateUserResponse createAuthenticateUserResponse() {
        return new AuthenticateUserResponse();
    }

    /**
     * Create an instance of {@link GetStatus }
     * 
     */
    public GetStatus createGetStatus() {
        return new GetStatus();
    }

    /**
     * Create an instance of {@link SearchResponse }
     * 
     */
    public SearchResponse createSearchResponse() {
        return new SearchResponse();
    }

    /**
     * Create an instance of {@link GetSearchLogPaged }
     * 
     */
    public GetSearchLogPaged createGetSearchLogPaged() {
        return new GetSearchLogPaged();
    }

    /**
     * Create an instance of {@link GetGuidResponse }
     * 
     */
    public GetGuidResponse createGetGuidResponse() {
        return new GetGuidResponse();
    }

    /**
     * Create an instance of {@link GetSetting }
     * 
     */
    public GetSetting createGetSetting() {
        return new GetSetting();
    }

    /**
     * Create an instance of {@link StopResponse }
     * 
     */
    public StopResponse createStopResponse() {
        return new StopResponse();
    }

    /**
     * Create an instance of {@link GetInterfaceVersionResponse }
     * 
     */
    public GetInterfaceVersionResponse createGetInterfaceVersionResponse() {
        return new GetInterfaceVersionResponse();
    }

    /**
     * Create an instance of {@link CreateFirstAdmin }
     * 
     */
    public CreateFirstAdmin createCreateFirstAdmin() {
        return new CreateFirstAdmin();
    }

    /**
     * Create an instance of {@link PingResponse }
     * 
     */
    public PingResponse createPingResponse() {
        return new PingResponse();
    }

    /**
     * Create an instance of {@link DeleteSystemLogResponse }
     * 
     */
    public DeleteSystemLogResponse createDeleteSystemLogResponse() {
        return new DeleteSystemLogResponse();
    }

    /**
     * Create an instance of {@link GetSearchLogResponse.GetSearchLogResult }
     * 
     */
    public GetSearchLogResponse.GetSearchLogResult createGetSearchLogResponseGetSearchLogResult() {
        return new GetSearchLogResponse.GetSearchLogResult();
    }

    /**
     * Create an instance of {@link GetSystemLogPaged }
     * 
     */
    public GetSystemLogPaged createGetSystemLogPaged() {
        return new GetSystemLogPaged();
    }

    /**
     * Create an instance of {@link SystemLogToken }
     * 
     */
    public SystemLogToken createSystemLogToken() {
        return new SystemLogToken();
    }

    /**
     * Create an instance of {@link GetPagedData }
     * 
     */
    public GetPagedData createGetPagedData() {
        return new GetPagedData();
    }

    /**
     * Create an instance of {@link SaveNotificationRecipient }
     * 
     */
    public SaveNotificationRecipient createSaveNotificationRecipient() {
        return new SaveNotificationRecipient();
    }

    /**
     * Create an instance of {@link GetUniqueSearchTagNamesResponse }
     * 
     */
    public GetUniqueSearchTagNamesResponse createGetUniqueSearchTagNamesResponse() {
        return new GetUniqueSearchTagNamesResponse();
    }

    /**
     * Create an instance of {@link ActivateLicenseKeyResponse }
     * 
     */
    public ActivateLicenseKeyResponse createActivateLicenseKeyResponse() {
        return new ActivateLicenseKeyResponse();
    }

    /**
     * Create an instance of {@link SaveCompanyInfo }
     * 
     */
    public SaveCompanyInfo createSaveCompanyInfo() {
        return new SaveCompanyInfo();
    }

    /**
     * Create an instance of {@link GetUsers }
     * 
     */
    public GetUsers createGetUsers() {
        return new GetUsers();
    }

    /**
     * Create an instance of {@link GetCompanyInfo }
     * 
     */
    public GetCompanyInfo createGetCompanyInfo() {
        return new GetCompanyInfo();
    }

    /**
     * Create an instance of {@link GetVersionResponse }
     * 
     */
    public GetVersionResponse createGetVersionResponse() {
        return new GetVersionResponse();
    }

    /**
     * Create an instance of {@link GetSigningCertsResponse }
     * 
     */
    public GetSigningCertsResponse createGetSigningCertsResponse() {
        return new GetSigningCertsResponse();
    }

    /**
     * Create an instance of {@link StartResponse }
     * 
     */
    public StartResponse createStartResponse() {
        return new StartResponse();
    }

    /**
     * Create an instance of {@link SaveCompanyInfoResponse }
     * 
     */
    public SaveCompanyInfoResponse createSaveCompanyInfoResponse() {
        return new SaveCompanyInfoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfInt }
     * 
     */
    public ArrayOfInt createArrayOfInt() {
        return new ArrayOfInt();
    }

    /**
     * Create an instance of {@link SearchTag }
     * 
     */
    public SearchTag createSearchTag() {
        return new SearchTag();
    }

    /**
     * Create an instance of {@link SearchRestrictionToken }
     * 
     */
    public SearchRestrictionToken createSearchRestrictionToken() {
        return new SearchRestrictionToken();
    }

    /**
     * Create an instance of {@link ArrayOfDataPage }
     * 
     */
    public ArrayOfDataPage createArrayOfDataPage() {
        return new ArrayOfDataPage();
    }

    /**
     * Create an instance of {@link PagedDataTable.Table }
     * 
     */
    public PagedDataTable.Table createPagedDataTableTable() {
        return new PagedDataTable.Table();
    }

}
