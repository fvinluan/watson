
package com.adriascan;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchRestrictionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SearchRestrictionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ShortMessages"/>
 *     &lt;enumeration value="LongMessages"/>
 *     &lt;enumeration value="Undetermined"/>
 *     &lt;enumeration value="CutOff"/>
 *     &lt;enumeration value="Silent"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SearchRestrictionType", namespace = "http://tempuri.org/")
@XmlEnum
public enum SearchRestrictionType {

    @XmlEnumValue("ShortMessages")
    SHORT_MESSAGES("ShortMessages"),
    @XmlEnumValue("LongMessages")
    LONG_MESSAGES("LongMessages"),
    @XmlEnumValue("Undetermined")
    UNDETERMINED("Undetermined"),
    @XmlEnumValue("CutOff")
    CUT_OFF("CutOff"),
    @XmlEnumValue("Silent")
    SILENT("Silent");
    private final String value;

    SearchRestrictionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SearchRestrictionType fromValue(String v) {
        for (SearchRestrictionType c: SearchRestrictionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
