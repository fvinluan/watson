
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPagedDataResult" type="{http://tempuri.org/}PagedDataTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPagedDataResult"
})
@XmlRootElement(name = "GetPagedDataResponse", namespace = "http://tempuri.org/")
public class GetPagedDataResponse {

    @XmlElement(name = "GetPagedDataResult", namespace = "http://tempuri.org/")
    protected PagedDataTable getPagedDataResult;

    /**
     * Gets the value of the getPagedDataResult property.
     * 
     * @return
     *     possible object is
     *     {@link PagedDataTable }
     *     
     */
    public PagedDataTable getGetPagedDataResult() {
        return getPagedDataResult;
    }

    /**
     * Sets the value of the getPagedDataResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PagedDataTable }
     *     
     */
    public void setGetPagedDataResult(PagedDataTable value) {
        this.getPagedDataResult = value;
    }

}
