
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSearchLogPagedResult" type="{http://tempuri.org/}PagedDataTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSearchLogPagedResult"
})
@XmlRootElement(name = "GetSearchLogPagedResponse", namespace = "http://tempuri.org/")
public class GetSearchLogPagedResponse {

    @XmlElement(name = "GetSearchLogPagedResult", namespace = "http://tempuri.org/")
    protected PagedDataTable getSearchLogPagedResult;

    /**
     * Gets the value of the getSearchLogPagedResult property.
     * 
     * @return
     *     possible object is
     *     {@link PagedDataTable }
     *     
     */
    public PagedDataTable getGetSearchLogPagedResult() {
        return getSearchLogPagedResult;
    }

    /**
     * Sets the value of the getSearchLogPagedResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PagedDataTable }
     *     
     */
    public void setGetSearchLogPagedResult(PagedDataTable value) {
        this.getSearchLogPagedResult = value;
    }

}
