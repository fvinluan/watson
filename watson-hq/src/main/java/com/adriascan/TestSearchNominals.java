
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="application" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="servicePassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="searchUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="searchPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referenceInCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scoreTreshold" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "username",
    "password",
    "application",
    "firstName",
    "lastName",
    "dateOfBirth",
    "url",
    "serviceUsername",
    "servicePassword",
    "serviceType",
    "searchUsername",
    "searchPassword",
    "referenceInCountry",
    "scoreTreshold"
})
@XmlRootElement(name = "TestSearchNominals", namespace = "http://tempuri.org/")
public class TestSearchNominals {

    @XmlElement(namespace = "http://tempuri.org/")
    protected String username;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String password;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String application;
    @XmlElement(name = "FirstName", namespace = "http://tempuri.org/")
    protected String firstName;
    @XmlElement(name = "LastName", namespace = "http://tempuri.org/")
    protected String lastName;
    @XmlElement(name = "DateOfBirth", namespace = "http://tempuri.org/", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String url;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String serviceUsername;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String servicePassword;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String serviceType;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String searchUsername;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String searchPassword;
    @XmlElement(namespace = "http://tempuri.org/")
    protected String referenceInCountry;
    @XmlElement(namespace = "http://tempuri.org/")
    protected int scoreTreshold;

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the application property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplication() {
        return application;
    }

    /**
     * Sets the value of the application property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplication(String value) {
        this.application = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the serviceUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceUsername() {
        return serviceUsername;
    }

    /**
     * Sets the value of the serviceUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceUsername(String value) {
        this.serviceUsername = value;
    }

    /**
     * Gets the value of the servicePassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePassword() {
        return servicePassword;
    }

    /**
     * Sets the value of the servicePassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePassword(String value) {
        this.servicePassword = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the searchUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchUsername() {
        return searchUsername;
    }

    /**
     * Sets the value of the searchUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchUsername(String value) {
        this.searchUsername = value;
    }

    /**
     * Gets the value of the searchPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchPassword() {
        return searchPassword;
    }

    /**
     * Sets the value of the searchPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchPassword(String value) {
        this.searchPassword = value;
    }

    /**
     * Gets the value of the referenceInCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceInCountry() {
        return referenceInCountry;
    }

    /**
     * Sets the value of the referenceInCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceInCountry(String value) {
        this.referenceInCountry = value;
    }

    /**
     * Gets the value of the scoreTreshold property.
     * 
     */
    public int getScoreTreshold() {
        return scoreTreshold;
    }

    /**
     * Sets the value of the scoreTreshold property.
     * 
     */
    public void setScoreTreshold(int value) {
        this.scoreTreshold = value;
    }

}
