
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSearchRestrictionsResult" type="{http://tempuri.org/}ArrayOfSearchRestrictionToken" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSearchRestrictionsResult"
})
@XmlRootElement(name = "GetSearchRestrictionsResponse", namespace = "http://tempuri.org/")
public class GetSearchRestrictionsResponse {

    @XmlElement(name = "GetSearchRestrictionsResult", namespace = "http://tempuri.org/")
    protected ArrayOfSearchRestrictionToken getSearchRestrictionsResult;

    /**
     * Gets the value of the getSearchRestrictionsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSearchRestrictionToken }
     *     
     */
    public ArrayOfSearchRestrictionToken getGetSearchRestrictionsResult() {
        return getSearchRestrictionsResult;
    }

    /**
     * Sets the value of the getSearchRestrictionsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSearchRestrictionToken }
     *     
     */
    public void setGetSearchRestrictionsResult(ArrayOfSearchRestrictionToken value) {
        this.getSearchRestrictionsResult = value;
    }

}
