
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TestSearchNominalsResult" type="{http://tempuri.org/}SearchDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "testSearchNominalsResult"
})
@XmlRootElement(name = "TestSearchNominalsResponse", namespace = "http://tempuri.org/")
public class TestSearchNominalsResponse {

    @XmlElement(name = "TestSearchNominalsResult", namespace = "http://tempuri.org/")
    protected SearchDetails testSearchNominalsResult;

    /**
     * Gets the value of the testSearchNominalsResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDetails }
     *     
     */
    public SearchDetails getTestSearchNominalsResult() {
        return testSearchNominalsResult;
    }

    /**
     * Sets the value of the testSearchNominalsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDetails }
     *     
     */
    public void setTestSearchNominalsResult(SearchDetails value) {
        this.testSearchNominalsResult = value;
    }

}
