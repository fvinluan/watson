
package com.adriascan;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchLogTokenResult.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SearchLogTokenResult">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="Positive"/>
 *     &lt;enumeration value="Negative"/>
 *     &lt;enumeration value="Errors"/>
 *     &lt;enumeration value="Undetermined"/>
 *     &lt;enumeration value="Restricted"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SearchLogTokenResult", namespace = "http://tempuri.org/")
@XmlEnum
public enum SearchLogTokenResult {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Positive")
    POSITIVE("Positive"),
    @XmlEnumValue("Negative")
    NEGATIVE("Negative"),
    @XmlEnumValue("Errors")
    ERRORS("Errors"),
    @XmlEnumValue("Undetermined")
    UNDETERMINED("Undetermined"),
    @XmlEnumValue("Restricted")
    RESTRICTED("Restricted");
    private final String value;

    SearchLogTokenResult(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SearchLogTokenResult fromValue(String v) {
        for (SearchLogTokenResult c: SearchLogTokenResult.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
