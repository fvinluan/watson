
package com.adriascan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for PagedDataTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PagedDataTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Table" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;any processContents='lax' namespace='http://www.w3.org/2001/XMLSchema' maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;any processContents='lax' namespace='urn:schemas-microsoft-com:xml-diffgram-v1'/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CurrentPage" type="{http://tempuri.org/}DataPage" minOccurs="0"/>
 *         &lt;element name="AvailablePages" type="{http://tempuri.org/}ArrayOfDataPage" minOccurs="0"/>
 *         &lt;element name="Summaries" type="{http://tempuri.org/}ArrayOfInt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagedDataTable", namespace = "http://tempuri.org/", propOrder = {
    "table",
    "currentPage",
    "availablePages",
    "summaries"
})
public class PagedDataTable {

    @XmlElement(name = "Table", namespace = "http://tempuri.org/")
    protected PagedDataTable.Table table;
    @XmlElement(name = "CurrentPage", namespace = "http://tempuri.org/")
    protected DataPage currentPage;
    @XmlElement(name = "AvailablePages", namespace = "http://tempuri.org/")
    protected ArrayOfDataPage availablePages;
    @XmlElement(name = "Summaries", namespace = "http://tempuri.org/")
    protected ArrayOfInt summaries;

    /**
     * Gets the value of the table property.
     * 
     * @return
     *     possible object is
     *     {@link PagedDataTable.Table }
     *     
     */
    public PagedDataTable.Table getTable() {
        return table;
    }

    /**
     * Sets the value of the table property.
     * 
     * @param value
     *     allowed object is
     *     {@link PagedDataTable.Table }
     *     
     */
    public void setTable(PagedDataTable.Table value) {
        this.table = value;
    }

    /**
     * Gets the value of the currentPage property.
     * 
     * @return
     *     possible object is
     *     {@link DataPage }
     *     
     */
    public DataPage getCurrentPage() {
        return currentPage;
    }

    /**
     * Sets the value of the currentPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataPage }
     *     
     */
    public void setCurrentPage(DataPage value) {
        this.currentPage = value;
    }

    /**
     * Gets the value of the availablePages property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDataPage }
     *     
     */
    public ArrayOfDataPage getAvailablePages() {
        return availablePages;
    }

    /**
     * Sets the value of the availablePages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDataPage }
     *     
     */
    public void setAvailablePages(ArrayOfDataPage value) {
        this.availablePages = value;
    }

    /**
     * Gets the value of the summaries property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInt }
     *     
     */
    public ArrayOfInt getSummaries() {
        return summaries;
    }

    /**
     * Sets the value of the summaries property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInt }
     *     
     */
    public void setSummaries(ArrayOfInt value) {
        this.summaries = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;any processContents='lax' namespace='http://www.w3.org/2001/XMLSchema' maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;any processContents='lax' namespace='urn:schemas-microsoft-com:xml-diffgram-v1'/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "anies"
    })
    public static class Table {

        @XmlAnyElement
        protected List<Element> anies;

        /**
         * Gets the value of the anies property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the anies property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAnies().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Element }
         * 
         * 
         */
        public List<Element> getAnies() {
            if (anies == null) {
                anies = new ArrayList<Element>();
            }
            return this.anies;
        }

    }

}
