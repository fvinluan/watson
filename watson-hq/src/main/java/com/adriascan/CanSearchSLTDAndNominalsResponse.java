
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CanSearchSLTDAndNominalsResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "canSearchSLTDAndNominalsResult"
})
@XmlRootElement(name = "CanSearchSLTDAndNominalsResponse", namespace = "http://tempuri.org/")
public class CanSearchSLTDAndNominalsResponse {

    @XmlElement(name = "CanSearchSLTDAndNominalsResult", namespace = "http://tempuri.org/")
    protected boolean canSearchSLTDAndNominalsResult;

    /**
     * Gets the value of the canSearchSLTDAndNominalsResult property.
     * 
     */
    public boolean isCanSearchSLTDAndNominalsResult() {
        return canSearchSLTDAndNominalsResult;
    }

    /**
     * Sets the value of the canSearchSLTDAndNominalsResult property.
     * 
     */
    public void setCanSearchSLTDAndNominalsResult(boolean value) {
        this.canSearchSLTDAndNominalsResult = value;
    }

}
