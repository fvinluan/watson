
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCompanyInfoResult" type="{http://tempuri.org/}CompanyInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCompanyInfoResult"
})
@XmlRootElement(name = "GetCompanyInfoResponse", namespace = "http://tempuri.org/")
public class GetCompanyInfoResponse {

    @XmlElement(name = "GetCompanyInfoResult", namespace = "http://tempuri.org/")
    protected CompanyInfo getCompanyInfoResult;

    /**
     * Gets the value of the getCompanyInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link CompanyInfo }
     *     
     */
    public CompanyInfo getGetCompanyInfoResult() {
        return getCompanyInfoResult;
    }

    /**
     * Sets the value of the getCompanyInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompanyInfo }
     *     
     */
    public void setGetCompanyInfoResult(CompanyInfo value) {
        this.getCompanyInfoResult = value;
    }

}
