
package com.adriascan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDataPage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDataPage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataPage" type="{http://tempuri.org/}DataPage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDataPage", namespace = "http://tempuri.org/", propOrder = {
    "dataPages"
})
public class ArrayOfDataPage {

    @XmlElement(name = "DataPage", namespace = "http://tempuri.org/", nillable = true)
    protected List<DataPage> dataPages;

    /**
     * Gets the value of the dataPages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataPages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataPages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataPage }
     * 
     * 
     */
    public List<DataPage> getDataPages() {
        if (dataPages == null) {
            dataPages = new ArrayList<DataPage>();
        }
        return this.dataPages;
    }

}
