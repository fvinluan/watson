
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApplicationUserToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicationUserToken">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RoleID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ResultsMode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UserInfoUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserInfoCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactPerson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactPersonEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnableSLTD" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EnableTDAWN" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EnableNominals" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="EnableSLTDAndNom" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ReceivePositiveNotification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SpecificCredentials" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IwsUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IwsPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SearchRestrictions" type="{http://tempuri.org/}ArrayOfSearchRestrictionToken" minOccurs="0"/>
 *         &lt;element name="SkipDetails" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="MergeRestrictedUndetermined" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicationUserToken", namespace = "http://tempuri.org/", propOrder = {
    "userID",
    "userName",
    "userPassword",
    "roleID",
    "isActive",
    "resultsMode",
    "userInfoUsername",
    "userInfoCountry",
    "address",
    "city",
    "contactPerson",
    "contactPersonEmail",
    "enableSLTD",
    "enableTDAWN",
    "enableNominals",
    "enableSLTDAndNom",
    "receivePositiveNotification",
    "specificCredentials",
    "iwsUsername",
    "iwsPassword",
    "countryReference",
    "searchRestrictions",
    "skipDetails",
    "mergeRestrictedUndetermined"
})
public class ApplicationUserToken {

    @XmlElement(name = "UserID", namespace = "http://tempuri.org/")
    protected long userID;
    @XmlElement(name = "UserName", namespace = "http://tempuri.org/")
    protected String userName;
    @XmlElement(name = "UserPassword", namespace = "http://tempuri.org/")
    protected String userPassword;
    @XmlElement(name = "RoleID", namespace = "http://tempuri.org/")
    protected long roleID;
    @XmlElement(name = "IsActive", namespace = "http://tempuri.org/")
    protected boolean isActive;
    @XmlElement(name = "ResultsMode", namespace = "http://tempuri.org/")
    protected int resultsMode;
    @XmlElement(name = "UserInfoUsername", namespace = "http://tempuri.org/")
    protected String userInfoUsername;
    @XmlElement(name = "UserInfoCountry", namespace = "http://tempuri.org/")
    protected String userInfoCountry;
    @XmlElement(name = "Address", namespace = "http://tempuri.org/")
    protected String address;
    @XmlElement(name = "City", namespace = "http://tempuri.org/")
    protected String city;
    @XmlElement(name = "ContactPerson", namespace = "http://tempuri.org/")
    protected String contactPerson;
    @XmlElement(name = "ContactPersonEmail", namespace = "http://tempuri.org/")
    protected String contactPersonEmail;
    @XmlElement(name = "EnableSLTD", namespace = "http://tempuri.org/")
    protected boolean enableSLTD;
    @XmlElement(name = "EnableTDAWN", namespace = "http://tempuri.org/")
    protected boolean enableTDAWN;
    @XmlElement(name = "EnableNominals", namespace = "http://tempuri.org/")
    protected boolean enableNominals;
    @XmlElement(name = "EnableSLTDAndNom", namespace = "http://tempuri.org/")
    protected boolean enableSLTDAndNom;
    @XmlElement(name = "ReceivePositiveNotification", namespace = "http://tempuri.org/")
    protected boolean receivePositiveNotification;
    @XmlElement(name = "SpecificCredentials", namespace = "http://tempuri.org/")
    protected boolean specificCredentials;
    @XmlElement(name = "IwsUsername", namespace = "http://tempuri.org/")
    protected String iwsUsername;
    @XmlElement(name = "IwsPassword", namespace = "http://tempuri.org/")
    protected String iwsPassword;
    @XmlElement(name = "CountryReference", namespace = "http://tempuri.org/")
    protected String countryReference;
    @XmlElement(name = "SearchRestrictions", namespace = "http://tempuri.org/")
    protected ArrayOfSearchRestrictionToken searchRestrictions;
    @XmlElement(name = "SkipDetails", namespace = "http://tempuri.org/")
    protected boolean skipDetails;
    @XmlElement(name = "MergeRestrictedUndetermined", namespace = "http://tempuri.org/")
    protected boolean mergeRestrictedUndetermined;

    /**
     * Gets the value of the userID property.
     * 
     */
    public long getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     */
    public void setUserID(long value) {
        this.userID = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the userPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * Sets the value of the userPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserPassword(String value) {
        this.userPassword = value;
    }

    /**
     * Gets the value of the roleID property.
     * 
     */
    public long getRoleID() {
        return roleID;
    }

    /**
     * Sets the value of the roleID property.
     * 
     */
    public void setRoleID(long value) {
        this.roleID = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     */
    public void setIsActive(boolean value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the resultsMode property.
     * 
     */
    public int getResultsMode() {
        return resultsMode;
    }

    /**
     * Sets the value of the resultsMode property.
     * 
     */
    public void setResultsMode(int value) {
        this.resultsMode = value;
    }

    /**
     * Gets the value of the userInfoUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserInfoUsername() {
        return userInfoUsername;
    }

    /**
     * Sets the value of the userInfoUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserInfoUsername(String value) {
        this.userInfoUsername = value;
    }

    /**
     * Gets the value of the userInfoCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserInfoCountry() {
        return userInfoCountry;
    }

    /**
     * Sets the value of the userInfoCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserInfoCountry(String value) {
        this.userInfoCountry = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the contactPerson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * Sets the value of the contactPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPerson(String value) {
        this.contactPerson = value;
    }

    /**
     * Gets the value of the contactPersonEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    /**
     * Sets the value of the contactPersonEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactPersonEmail(String value) {
        this.contactPersonEmail = value;
    }

    /**
     * Gets the value of the enableSLTD property.
     * 
     */
    public boolean isEnableSLTD() {
        return enableSLTD;
    }

    /**
     * Sets the value of the enableSLTD property.
     * 
     */
    public void setEnableSLTD(boolean value) {
        this.enableSLTD = value;
    }

    /**
     * Gets the value of the enableTDAWN property.
     * 
     */
    public boolean isEnableTDAWN() {
        return enableTDAWN;
    }

    /**
     * Sets the value of the enableTDAWN property.
     * 
     */
    public void setEnableTDAWN(boolean value) {
        this.enableTDAWN = value;
    }

    /**
     * Gets the value of the enableNominals property.
     * 
     */
    public boolean isEnableNominals() {
        return enableNominals;
    }

    /**
     * Sets the value of the enableNominals property.
     * 
     */
    public void setEnableNominals(boolean value) {
        this.enableNominals = value;
    }

    /**
     * Gets the value of the enableSLTDAndNom property.
     * 
     */
    public boolean isEnableSLTDAndNom() {
        return enableSLTDAndNom;
    }

    /**
     * Sets the value of the enableSLTDAndNom property.
     * 
     */
    public void setEnableSLTDAndNom(boolean value) {
        this.enableSLTDAndNom = value;
    }

    /**
     * Gets the value of the receivePositiveNotification property.
     * 
     */
    public boolean isReceivePositiveNotification() {
        return receivePositiveNotification;
    }

    /**
     * Sets the value of the receivePositiveNotification property.
     * 
     */
    public void setReceivePositiveNotification(boolean value) {
        this.receivePositiveNotification = value;
    }

    /**
     * Gets the value of the specificCredentials property.
     * 
     */
    public boolean isSpecificCredentials() {
        return specificCredentials;
    }

    /**
     * Sets the value of the specificCredentials property.
     * 
     */
    public void setSpecificCredentials(boolean value) {
        this.specificCredentials = value;
    }

    /**
     * Gets the value of the iwsUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIwsUsername() {
        return iwsUsername;
    }

    /**
     * Sets the value of the iwsUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIwsUsername(String value) {
        this.iwsUsername = value;
    }

    /**
     * Gets the value of the iwsPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIwsPassword() {
        return iwsPassword;
    }

    /**
     * Sets the value of the iwsPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIwsPassword(String value) {
        this.iwsPassword = value;
    }

    /**
     * Gets the value of the countryReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryReference() {
        return countryReference;
    }

    /**
     * Sets the value of the countryReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryReference(String value) {
        this.countryReference = value;
    }

    /**
     * Gets the value of the searchRestrictions property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSearchRestrictionToken }
     *     
     */
    public ArrayOfSearchRestrictionToken getSearchRestrictions() {
        return searchRestrictions;
    }

    /**
     * Sets the value of the searchRestrictions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSearchRestrictionToken }
     *     
     */
    public void setSearchRestrictions(ArrayOfSearchRestrictionToken value) {
        this.searchRestrictions = value;
    }

    /**
     * Gets the value of the skipDetails property.
     * 
     */
    public boolean isSkipDetails() {
        return skipDetails;
    }

    /**
     * Sets the value of the skipDetails property.
     * 
     */
    public void setSkipDetails(boolean value) {
        this.skipDetails = value;
    }

    /**
     * Gets the value of the mergeRestrictedUndetermined property.
     * 
     */
    public boolean isMergeRestrictedUndetermined() {
        return mergeRestrictedUndetermined;
    }

    /**
     * Sets the value of the mergeRestrictedUndetermined property.
     * 
     */
    public void setMergeRestrictedUndetermined(boolean value) {
        this.mergeRestrictedUndetermined = value;
    }

}
