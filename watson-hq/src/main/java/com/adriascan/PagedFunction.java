
package com.adriascan;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PagedFunction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PagedFunction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SearchLog"/>
 *     &lt;enumeration value="SystemLog"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PagedFunction", namespace = "http://tempuri.org/")
@XmlEnum
public enum PagedFunction {

    @XmlEnumValue("SearchLog")
    SEARCH_LOG("SearchLog"),
    @XmlEnumValue("SystemLog")
    SYSTEM_LOG("SystemLog");
    private final String value;

    PagedFunction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PagedFunction fromValue(String v) {
        for (PagedFunction c: PagedFunction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
