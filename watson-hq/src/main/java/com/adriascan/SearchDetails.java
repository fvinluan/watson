
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsPositive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ResultCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResultOtherCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RequestID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceInCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XMLData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdditionalData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPositive_Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchDetails", namespace = "http://tempuri.org/", propOrder = {
    "isPositive",
    "resultCode",
    "resultOtherCode",
    "requestID",
    "referenceInCountry",
    "xmlData",
    "additionalData",
    "isPositiveText",
    "description"
})
public class SearchDetails {

    @XmlElement(name = "IsPositive", namespace = "http://tempuri.org/", required = true, type = Boolean.class, nillable = true)
    protected Boolean isPositive;
    @XmlElement(name = "ResultCode", namespace = "http://tempuri.org/")
    protected String resultCode;
    @XmlElement(name = "ResultOtherCode", namespace = "http://tempuri.org/")
    protected String resultOtherCode;
    @XmlElement(name = "RequestID", namespace = "http://tempuri.org/")
    protected String requestID;
    @XmlElement(name = "ReferenceInCountry", namespace = "http://tempuri.org/")
    protected String referenceInCountry;
    @XmlElement(name = "XMLData", namespace = "http://tempuri.org/")
    protected String xmlData;
    @XmlElement(name = "AdditionalData", namespace = "http://tempuri.org/")
    protected String additionalData;
    @XmlElement(name = "IsPositive_Text", namespace = "http://tempuri.org/")
    protected String isPositiveText;
    @XmlElement(name = "Description", namespace = "http://tempuri.org/")
    protected String description;

    /**
     * Gets the value of the isPositive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPositive() {
        return isPositive;
    }

    /**
     * Sets the value of the isPositive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPositive(Boolean value) {
        this.isPositive = value;
    }

    /**
     * Gets the value of the resultCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultCode(String value) {
        this.resultCode = value;
    }

    /**
     * Gets the value of the resultOtherCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultOtherCode() {
        return resultOtherCode;
    }

    /**
     * Sets the value of the resultOtherCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultOtherCode(String value) {
        this.resultOtherCode = value;
    }

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the referenceInCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceInCountry() {
        return referenceInCountry;
    }

    /**
     * Sets the value of the referenceInCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceInCountry(String value) {
        this.referenceInCountry = value;
    }

    /**
     * Gets the value of the xmlData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXMLData() {
        return xmlData;
    }

    /**
     * Sets the value of the xmlData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXMLData(String value) {
        this.xmlData = value;
    }

    /**
     * Gets the value of the additionalData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalData() {
        return additionalData;
    }

    /**
     * Sets the value of the additionalData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalData(String value) {
        this.additionalData = value;
    }

    /**
     * Gets the value of the isPositiveText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPositiveText() {
        return isPositiveText;
    }

    /**
     * Sets the value of the isPositiveText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPositiveText(String value) {
        this.isPositiveText = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
