
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TestSearchResult" type="{http://tempuri.org/}SearchDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "testSearchResult"
})
@XmlRootElement(name = "TestSearchResponse", namespace = "http://tempuri.org/")
public class TestSearchResponse {

    @XmlElement(name = "TestSearchResult", namespace = "http://tempuri.org/")
    protected SearchDetails testSearchResult;

    /**
     * Gets the value of the testSearchResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDetails }
     *     
     */
    public SearchDetails getTestSearchResult() {
        return testSearchResult;
    }

    /**
     * Sets the value of the testSearchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDetails }
     *     
     */
    public void setTestSearchResult(SearchDetails value) {
        this.testSearchResult = value;
    }

}
