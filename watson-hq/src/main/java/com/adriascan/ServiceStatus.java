
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Inizialized" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Licensed" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceStatus", namespace = "http://tempuri.org/", propOrder = {
    "inizialized",
    "licensed"
})
public class ServiceStatus {

    @XmlElement(name = "Inizialized", namespace = "http://tempuri.org/")
    protected boolean inizialized;
    @XmlElement(name = "Licensed", namespace = "http://tempuri.org/")
    protected boolean licensed;

    /**
     * Gets the value of the inizialized property.
     * 
     */
    public boolean isInizialized() {
        return inizialized;
    }

    /**
     * Sets the value of the inizialized property.
     * 
     */
    public void setInizialized(boolean value) {
        this.inizialized = value;
    }

    /**
     * Gets the value of the licensed property.
     * 
     */
    public boolean isLicensed() {
        return licensed;
    }

    /**
     * Sets the value of the licensed property.
     * 
     */
    public void setLicensed(boolean value) {
        this.licensed = value;
    }

}
