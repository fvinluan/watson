
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchNominalsResult" type="{http://tempuri.org/}SearchDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchNominalsResult"
})
@XmlRootElement(name = "SearchNominalsResponse", namespace = "http://tempuri.org/")
public class SearchNominalsResponse {

    @XmlElement(name = "SearchNominalsResult", namespace = "http://tempuri.org/")
    protected SearchDetails searchNominalsResult;

    /**
     * Gets the value of the searchNominalsResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDetails }
     *     
     */
    public SearchDetails getSearchNominalsResult() {
        return searchNominalsResult;
    }

    /**
     * Sets the value of the searchNominalsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDetails }
     *     
     */
    public void setSearchNominalsResult(SearchDetails value) {
        this.searchNominalsResult = value;
    }

}
