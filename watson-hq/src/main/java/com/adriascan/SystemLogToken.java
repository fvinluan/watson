
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SystemLogToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SystemLogToken">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="From" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="To" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="MessageFilter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MessageFilterModeInclude" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemLogToken", namespace = "http://tempuri.org/", propOrder = {
    "from",
    "to",
    "messageFilter",
    "messageFilterModeInclude"
})
public class SystemLogToken {

    @XmlElement(name = "From", namespace = "http://tempuri.org/", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar from;
    @XmlElement(name = "To", namespace = "http://tempuri.org/", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar to;
    @XmlElement(name = "MessageFilter", namespace = "http://tempuri.org/")
    protected String messageFilter;
    @XmlElement(name = "MessageFilterModeInclude", namespace = "http://tempuri.org/")
    protected boolean messageFilterModeInclude;

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFrom(XMLGregorianCalendar value) {
        this.from = value;
    }

    /**
     * Gets the value of the to property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTo() {
        return to;
    }

    /**
     * Sets the value of the to property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTo(XMLGregorianCalendar value) {
        this.to = value;
    }

    /**
     * Gets the value of the messageFilter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageFilter() {
        return messageFilter;
    }

    /**
     * Sets the value of the messageFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageFilter(String value) {
        this.messageFilter = value;
    }

    /**
     * Gets the value of the messageFilterModeInclude property.
     * 
     */
    public boolean isMessageFilterModeInclude() {
        return messageFilterModeInclude;
    }

    /**
     * Sets the value of the messageFilterModeInclude property.
     * 
     */
    public void setMessageFilterModeInclude(boolean value) {
        this.messageFilterModeInclude = value;
    }

}
