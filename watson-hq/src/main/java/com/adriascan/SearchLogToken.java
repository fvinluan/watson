
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SearchLogToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchLogToken">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="From" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="To" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SearchUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Result" type="{http://tempuri.org/}SearchLogTokenResult"/>
 *         &lt;element name="PositiveServices" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tags" type="{http://tempuri.org/}ArrayOfSearchTag" minOccurs="0"/>
 *         &lt;element name="FillTagsColumn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchLogToken", namespace = "http://tempuri.org/", propOrder = {
    "from",
    "to",
    "din",
    "country",
    "docType",
    "firstName",
    "lastName",
    "dateOfBirth",
    "searchUsername",
    "result",
    "positiveServices",
    "referenceNumber",
    "tags",
    "fillTagsColumn"
})
public class SearchLogToken {

    @XmlElement(name = "From", namespace = "http://tempuri.org/", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar from;
    @XmlElement(name = "To", namespace = "http://tempuri.org/", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar to;
    @XmlElement(name = "DIN", namespace = "http://tempuri.org/")
    protected String din;
    @XmlElement(name = "Country", namespace = "http://tempuri.org/")
    protected String country;
    @XmlElement(name = "DocType", namespace = "http://tempuri.org/")
    protected String docType;
    @XmlElement(name = "FirstName", namespace = "http://tempuri.org/")
    protected String firstName;
    @XmlElement(name = "LastName", namespace = "http://tempuri.org/")
    protected String lastName;
    @XmlElement(name = "DateOfBirth", namespace = "http://tempuri.org/", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(name = "SearchUsername", namespace = "http://tempuri.org/")
    protected String searchUsername;
    @XmlElement(name = "Result", namespace = "http://tempuri.org/", required = true)
    protected SearchLogTokenResult result;
    @XmlElement(name = "PositiveServices", namespace = "http://tempuri.org/")
    protected String positiveServices;
    @XmlElement(name = "ReferenceNumber", namespace = "http://tempuri.org/")
    protected String referenceNumber;
    @XmlElement(name = "Tags", namespace = "http://tempuri.org/")
    protected ArrayOfSearchTag tags;
    @XmlElement(name = "FillTagsColumn", namespace = "http://tempuri.org/")
    protected boolean fillTagsColumn;

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFrom(XMLGregorianCalendar value) {
        this.from = value;
    }

    /**
     * Gets the value of the to property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTo() {
        return to;
    }

    /**
     * Sets the value of the to property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTo(XMLGregorianCalendar value) {
        this.to = value;
    }

    /**
     * Gets the value of the din property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDIN() {
        return din;
    }

    /**
     * Sets the value of the din property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDIN(String value) {
        this.din = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the docType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the value of the docType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocType(String value) {
        this.docType = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the searchUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchUsername() {
        return searchUsername;
    }

    /**
     * Sets the value of the searchUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchUsername(String value) {
        this.searchUsername = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link SearchLogTokenResult }
     *     
     */
    public SearchLogTokenResult getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchLogTokenResult }
     *     
     */
    public void setResult(SearchLogTokenResult value) {
        this.result = value;
    }

    /**
     * Gets the value of the positiveServices property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPositiveServices() {
        return positiveServices;
    }

    /**
     * Sets the value of the positiveServices property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPositiveServices(String value) {
        this.positiveServices = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the tags property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSearchTag }
     *     
     */
    public ArrayOfSearchTag getTags() {
        return tags;
    }

    /**
     * Sets the value of the tags property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSearchTag }
     *     
     */
    public void setTags(ArrayOfSearchTag value) {
        this.tags = value;
    }

    /**
     * Gets the value of the fillTagsColumn property.
     * 
     */
    public boolean isFillTagsColumn() {
        return fillTagsColumn;
    }

    /**
     * Sets the value of the fillTagsColumn property.
     * 
     */
    public void setFillTagsColumn(boolean value) {
        this.fillTagsColumn = value;
    }

}
