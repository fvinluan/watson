
package com.adriascan;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSearchRestrictionToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSearchRestrictionToken">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchRestrictionToken" type="{http://tempuri.org/}SearchRestrictionToken" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSearchRestrictionToken", namespace = "http://tempuri.org/", propOrder = {
    "searchRestrictionTokens"
})
public class ArrayOfSearchRestrictionToken {

    @XmlElement(name = "SearchRestrictionToken", namespace = "http://tempuri.org/", nillable = true)
    protected List<SearchRestrictionToken> searchRestrictionTokens;

    /**
     * Gets the value of the searchRestrictionTokens property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the searchRestrictionTokens property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchRestrictionTokens().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchRestrictionToken }
     * 
     * 
     */
    public List<SearchRestrictionToken> getSearchRestrictionTokens() {
        if (searchRestrictionTokens == null) {
            searchRestrictionTokens = new ArrayList<SearchRestrictionToken>();
        }
        return this.searchRestrictionTokens;
    }

}
