
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SLTDNomSearchResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SLTDNomSearchResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SLTDResult" type="{http://tempuri.org/}SearchDetails" minOccurs="0"/>
 *         &lt;element name="NominalsResult" type="{http://tempuri.org/}SearchDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SLTDNomSearchResult", namespace = "http://tempuri.org/", propOrder = {
    "sltdResult",
    "nominalsResult"
})
public class SLTDNomSearchResult {

    @XmlElement(name = "SLTDResult", namespace = "http://tempuri.org/")
    protected SearchDetails sltdResult;
    @XmlElement(name = "NominalsResult", namespace = "http://tempuri.org/")
    protected SearchDetails nominalsResult;

    /**
     * Gets the value of the sltdResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDetails }
     *     
     */
    public SearchDetails getSLTDResult() {
        return sltdResult;
    }

    /**
     * Sets the value of the sltdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDetails }
     *     
     */
    public void setSLTDResult(SearchDetails value) {
        this.sltdResult = value;
    }

    /**
     * Gets the value of the nominalsResult property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDetails }
     *     
     */
    public SearchDetails getNominalsResult() {
        return nominalsResult;
    }

    /**
     * Sets the value of the nominalsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDetails }
     *     
     */
    public void setNominalsResult(SearchDetails value) {
        this.nominalsResult = value;
    }

}
