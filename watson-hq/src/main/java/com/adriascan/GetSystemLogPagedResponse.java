
package com.adriascan;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSystemLogPagedResult" type="{http://tempuri.org/}PagedDataTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSystemLogPagedResult"
})
@XmlRootElement(name = "GetSystemLogPagedResponse", namespace = "http://tempuri.org/")
public class GetSystemLogPagedResponse {

    @XmlElement(name = "GetSystemLogPagedResult", namespace = "http://tempuri.org/")
    protected PagedDataTable getSystemLogPagedResult;

    /**
     * Gets the value of the getSystemLogPagedResult property.
     * 
     * @return
     *     possible object is
     *     {@link PagedDataTable }
     *     
     */
    public PagedDataTable getGetSystemLogPagedResult() {
        return getSystemLogPagedResult;
    }

    /**
     * Sets the value of the getSystemLogPagedResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PagedDataTable }
     *     
     */
    public void setGetSystemLogPagedResult(PagedDataTable value) {
        this.getSystemLogPagedResult = value;
    }

}
