package com.broadwayharvey.watson.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.broadwayharvey.watson.model.SystemInformation;
import com.broadwayharvey.watson.repository.SystemInformationRepository;
import com.broadwayharvey.watson.service.SystemInformationService;
import com.google.common.collect.Lists;

@Service("systemInformationService")
@Transactional(readOnly = true)
public class SystemInformationServiceImpl extends BaseService
    implements SystemInformationService {
  @Autowired
  private SystemInformationRepository systemInformationRepository;

  @Override
  public SystemInformation findSystemInformation() {
    List<SystemInformation> systemInformationList =
        Lists.newArrayList(systemInformationRepository.findAll());

    // TODO : throw exception if not found
    return systemInformationList.get(0);
  }

  public void
      setSystemInformationRepository(SystemInformationRepository systemInformationRepository) {
    this.systemInformationRepository = systemInformationRepository;
  }

}
