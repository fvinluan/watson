package com.broadwayharvey.watson.service;

import com.broadwayharvey.watson.model.Verification;


/**
 */
public interface DocumentService
    extends Service {

  void verifyDocument(Verification verification);

}
