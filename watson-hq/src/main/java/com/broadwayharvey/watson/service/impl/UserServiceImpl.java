package com.broadwayharvey.watson.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.broadwayharvey.watson.model.User;
import com.broadwayharvey.watson.repository.UserRepository;
import com.broadwayharvey.watson.service.UserService;

@Service("userService")
@Transactional(readOnly = true)
public class UserServiceImpl extends BaseService
    implements UserService {
  @Autowired
  private UserRepository userRepository;

  @Override
  public User findUserByUserName(String username) {
    return userRepository.findByUsername(username);
  }

  @Override
  public List<User> findUsers() {
    return (List<User>) userRepository.findAll();
  }

  public void setUserRepository(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

}
