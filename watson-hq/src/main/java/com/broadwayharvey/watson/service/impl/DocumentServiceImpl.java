package com.broadwayharvey.watson.service.impl;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.adriascan.ArrayOfSearchTag;
import com.adriascan.ObjectFactory;
import com.adriascan.SearchDocument;
import com.adriascan.SearchDocumentResponse;
import com.adriascan.SearchDocumentResult;
import com.adriascan.SearchTag;
import com.broadwayharvey.watson.model.SystemInformation;
import com.broadwayharvey.watson.model.Verification;
import com.broadwayharvey.watson.repository.VerificationRepository;
import com.broadwayharvey.watson.service.DocumentService;
import com.broadwayharvey.watson.service.SystemInformationService;

/**
 */
@Service("documentService")
@Transactional(readOnly = true)
public class DocumentServiceImpl extends BaseService
    implements DocumentService {

  private static final Logger LOGGER = LoggerFactory.getLogger(DocumentServiceImpl.class);

  @Autowired
  protected WebServiceTemplate adriascanWebServiceTemplate;
  
  @Autowired
  VerificationRepository verificationRepository;

  @Autowired
  protected SystemInformationService systemInformationService;

//  @Transactional(value = "watson", readOnly = false, propagation = Propagation.REQUIRED,
//      rollbackFor = java.lang.Exception.class)
  public void verifyDocument(Verification verification) {
//    SearchDocumentResponse soapResponse = createSOAPRequest(verification);
//    SearchDocumentResult searchDocumentResult = soapResponse.getSearchDocumentResult();
//    XMLGregorianCalendar searchDate = searchDocumentResult.getSearchDate();
//    if (searchDate != null) {
//      verification.setSearchDate(searchDate.toGregorianCalendar().getTime());
//    }
//    verification.setResultCode(searchDocumentResult.getResultCode());
//    if (searchDocumentResult.isPositive() != null) {
//      verification.setPositive(searchDocumentResult.isPositive());
//    }
//    verification.setPositiveText(searchDocumentResult.getPositiveText());
//    verification.setDescription(searchDocumentResult.getDescription());
//    verification.setRestrictionType(searchDocumentResult.getRestrictionType());
    verificationRepository.save(verification);
  }

  private SearchDocumentResponse createSOAPRequest(Verification verification) {
    SearchDocument request = new ObjectFactory().createSearchDocument();

    SystemInformation systemInformation = systemInformationService.findSystemInformation();
    
    systemInformation.setVerificationAPIURI("http://192.168.58.35:8089/Fls_Customer_Test/fls.asmx");
    systemInformation.setVerificationAPIUsername("CarinvalCruise");
    systemInformation.setVerificationAPIPassword("CarinvalCruise.1234#");
    
    adriascanWebServiceTemplate.setDefaultUri(systemInformation.getVerificationAPIURI());
    request.setUsername(systemInformation.getVerificationAPIUsername());
    String password = systemInformation.getVerificationAPIPassword();
    // TODO : retieve passwork from db and decrypt
    // try {
    // password =
    // EncryptionUtil.decrypt(systemInformationService
    // .getSystemInformation(SystemInformationId.ICHECKIT_PASSWORD));
    // } catch (GeneralSecurityException | IOException e) {
    // throw new WatsonException(ExceptionCode.ERROR_DECRYPTING, e);
    // }
    request.setPassword(password);

    request.setDocumentNumber(verification.getDocumentNumber());
    request.setDocumentType(verification.getDocumentType());
    request.setCountryISO(verification.getIssuingCountry());
    request.setReferenceNumber(verification.getReferenceNumber());

    ArrayOfSearchTag arrayOfSearchTag = new ArrayOfSearchTag();
    List<com.broadwayharvey.watson.model.SearchTag> tags = verification.getTags();
    for (com.broadwayharvey.watson.model.SearchTag vtag : tags) {
      SearchTag t = new SearchTag();
      t.setTag(vtag.getTag());
      t.setValue(vtag.getValue());
      arrayOfSearchTag.getSearchTags().add(t);
    }
    request.setTags(arrayOfSearchTag);
    SearchDocumentResponse response =
        (SearchDocumentResponse) adriascanWebServiceTemplate.marshalSendAndReceive(request);


    return response;
  }

}
