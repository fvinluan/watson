package com.broadwayharvey.watson.service;

import java.util.List;

import com.broadwayharvey.watson.model.User;

public interface UserService
    extends Service {

  User findUserByUserName(String userName);

  List<User> findUsers();



}
