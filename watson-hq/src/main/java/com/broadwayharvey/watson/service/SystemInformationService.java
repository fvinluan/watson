package com.broadwayharvey.watson.service;

import java.math.BigInteger;

import com.broadwayharvey.watson.model.SystemInformation;

public interface SystemInformationService
    extends Service {

  SystemInformation findSystemInformation();

}
