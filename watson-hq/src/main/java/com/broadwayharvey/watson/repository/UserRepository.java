package com.broadwayharvey.watson.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.broadwayharvey.watson.model.User;

@Repository
public interface UserRepository
    extends CrudRepository<User, BigInteger> {

  public User findByUsername(String username);


}
