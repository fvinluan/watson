package com.broadwayharvey.watson.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.broadwayharvey.watson.model.Verification;

@Repository
public interface VerificationRepository
    extends CrudRepository<Verification, BigInteger> {

  public Verification findByDocumentNumberAndDocumentTypeAndIssuingCountry(String documentNumber,
      String documentType, String issuingCountry);

  public List<Verification> findByIssuingCountry(String issuingCountry);

  public List<Verification> findByDocumentType(String documentType);

}
