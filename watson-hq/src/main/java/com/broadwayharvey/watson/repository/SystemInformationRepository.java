package com.broadwayharvey.watson.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.broadwayharvey.watson.model.SystemInformation;

@Repository
public interface SystemInformationRepository
    extends CrudRepository<SystemInformation, BigInteger> {

  public SystemInformation findByid(BigInteger id);
  
  


}
