package com.broadwayharvey.watson.rabbitmq.listener;

import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.broadwayharvey.watson.model.Verification;
import com.broadwayharvey.watson.rabbitmq.exception.RepublishMessageRecovererWithNumberOfTriesException;
import com.broadwayharvey.watson.service.DocumentService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 */
public class RabbitListener
    implements MessageListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(RabbitListener.class);
  ObjectMapper mapper = new ObjectMapper();

  @Autowired
  DocumentService documentService;


  /**
   * Method onMessage.
   *
   * @param message Message
   * @see org.springframework.amqp.core.MessageListener#onMessage(Message)
   */
  @Override
  public void onMessage(Message message) {
    try {
      String receivedFrom =
          message.getMessageProperties().getHeaders().get("bindingKey").toString();
      String messageBody = StringUtils.newStringUtf8(message.getBody());
      Verification verification = mapper.readValue(messageBody, Verification.class);
      documentService.verifyDocument(verification);
      LOGGER.info("Retrieved from: {} | {}", receivedFrom, verification.toString());
      // FIXME : only republish for specific exception. if it is not needed to be caught then
      // swallow
    } catch (Exception e) {
      LOGGER.error("Error listening ", e);
      throw new RepublishMessageRecovererWithNumberOfTriesException(e);
    }
  }
}
