package com.broadwayharvey.watson.rabbitmq.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.broadwayharvey.watson.rabbitmq.service.MessageProducerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 */
public class MessageProducerServiceImpl implements MessageProducerService {

  private String bindingKey;

  @Autowired
  private RabbitTemplate rabbitTemplate;

  /**
   * Method buildMessageProperties.
   * @return MessageProperties
   */
  private MessageProperties buildMessageProperties() {
    MessageProperties props = new MessageProperties();
    props.setHeader("bindingKey", bindingKey);
    String messageId = UUID.randomUUID().toString();
    props.setMessageId(messageId);
    return props;
  }

  /**
   * Method getBindingKey.
   * @return String
   */
  public String getBindingKey() {
    return bindingKey;
  }

  /**
   * Method publishToExchange.
   * @param objectToSend Object
   * @param routingKey String
   * @throws UnsupportedEncodingException
   * @throws JsonProcessingException
   * @see com.broadwayharvey.watson.rabbitmq.service.MessageProducerService#publishToExchange(Object, String)
   */
  @Override
  public void publishToExchange(Object objectToSend, String routingKey)
      throws UnsupportedEncodingException, JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    String messageToSend = mapper.writeValueAsString(objectToSend);
    MessageProperties props = buildMessageProperties();
    Message message = new Message(messageToSend.getBytes("UTF-8"), props);
    rabbitTemplate.convertAndSend(routingKey, message);
  }

  /**
   * Method setBindingKey.
   * @param bindingKey String
   */
  public void setBindingKey(String bindingKey) {
    this.bindingKey = bindingKey;
  }

  /**
   * Method setRabbitTemplate.
   * @param rabbitTemplate RabbitTemplate
   */
  public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;
  }

}
