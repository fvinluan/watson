package com.broadwayharvey.watson.rabbitmq.exception;

/**
 */
public class RepublishMessageRecovererWithNumberOfTriesException extends RuntimeException {

  /**
     * 
     */
  private static final long serialVersionUID = 1580576228907025789L;

  /**
   * Constructor for RepublishMessageRecovererWithNumberOfTriesException.
   * @param cause Throwable
   */
  public RepublishMessageRecovererWithNumberOfTriesException(Throwable cause) {
    super(cause);
  }

}
