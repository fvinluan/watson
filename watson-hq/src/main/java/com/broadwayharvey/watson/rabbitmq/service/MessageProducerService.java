package com.broadwayharvey.watson.rabbitmq.service;

import java.io.IOException;

/**
 */
public interface MessageProducerService {

  /**
   * Method publishToExchange.
   * @param objectToSend Object
   * @param routingKey String
   * @throws IOException
   */
  void publishToExchange(Object objectToSend, String routingKey) throws IOException;

}
