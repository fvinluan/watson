package com.broadwayharvey.watson.rabbitmq.override;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer;

/**
 */
public class RepublishMessageRecovererWithNumberOfTries extends RepublishMessageRecoverer {
  private static final Logger LOGGER = LoggerFactory
      .getLogger(RepublishMessageRecovererWithNumberOfTries.class);

  /**
   * Constructor for RepublishMessageRecovererWithNumberOfTries.
   * @param errorTemplate AmqpTemplate
   * @param errorExchange String
   * @param errorRoutingKey String
   */
  public RepublishMessageRecovererWithNumberOfTries(AmqpTemplate errorTemplate,
      String errorExchange, String errorRoutingKey) {
    super(errorTemplate, errorExchange, errorRoutingKey);
  }

  /**
   * Method recover.
   * @param message Message
   * @param cause Throwable
   * @see org.springframework.amqp.rabbit.retry.MessageRecoverer#recover(Message, Throwable)
   */
  @Override
  public void recover(Message message, Throwable cause) {
    if (message.getMessageProperties().getHeaders().get("numberOfTries") != null) {
      int numberOfTries =
          NumberUtils.toInt(message.getMessageProperties().getHeaders().get("numberOfTries")
              .toString(), 1);
      if (numberOfTries % 10 == 0) {
        LOGGER.warn("Send an email. Number of times through queue: {}", numberOfTries);
      }
      ++numberOfTries;
      message.getMessageProperties().setHeader("numberOfTries", numberOfTries);
    } else {
      message.getMessageProperties().setHeader("numberOfTries", "1");
    }
    super.recover(message, cause);
  }

}
