package com.broadwayharvey.watson.service.impl;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.beanutils.converters.LongConverter;

import com.broadwayharvey.watson.service.Service;

/**
 * Base class for Business Services - use this class for utility methods and generic CRUD methods.
 * 
 * @author <a href="mailto:fvinluan@gmail.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
public class BaseService
    implements Service {
  private static final Long defaultLong = null;

  static {
    ConvertUtils.register(new LongConverter(defaultLong), Long.class);
    ConvertUtils.register(new IntegerConverter(defaultLong), Integer.class);
  }



}
