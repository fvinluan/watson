package com.broadwayharvey.watson.service;

import com.broadwayharvey.watson.model.Verification;

/**
 */
public interface VerificationService
    extends Service {

  void postVerificationRequest(Verification verification) throws Exception;

}
