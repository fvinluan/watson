package com.broadwayharvey.watson.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.broadwayharvey.watson.model.Verification;
import com.broadwayharvey.watson.rabbitmq.service.MessageProducerService;
import com.broadwayharvey.watson.service.VerificationService;

/**
 */
@Service("verificationService")
@Transactional(readOnly = true)
public class VerificationServiceImpl extends BaseService
    implements VerificationService {

  private static final Logger LOGGER = LoggerFactory.getLogger(VerificationServiceImpl.class);

  @Autowired
  private MessageProducerService messageProducerService;

  @Override
  public void postVerificationRequest(Verification verification) throws Exception {
    if (LOGGER.isInfoEnabled()){
      LOGGER.info("Start Submitting {} for interpol verification ", verification.toString());
    }
    messageProducerService.publishToExchange(verification);
    if (LOGGER.isInfoEnabled()){
      LOGGER.info("Finished Submitting {} for interpol verification ", verification.toString());
    }
  }
}
