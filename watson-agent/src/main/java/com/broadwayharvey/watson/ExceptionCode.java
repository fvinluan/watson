package com.broadwayharvey.watson;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author <a href="mailto:fvinluan@gmail.com">Francis Vinluan</a>
 * 
 * @version $Revision: 1.0 $
 */
public enum ExceptionCode {
  MALFORMED_REQUEST(
      "malformed.request",
      HttpStatus.FORBIDDEN,
      8003,
      "Malformed Request");

  private int code;
  private String description;
  private HttpStatus httpStatus;
  private String key;

  /**
   * 
   * @param key
   * @param description
   */
  private ExceptionCode(String key, HttpStatus httpStatus, int code, String description) {
    this.key = key;
    this.description = description;
    this.code = code;
    this.httpStatus = httpStatus;
  }

  public int getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

  public String getKey() {
    return key;
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    return key;
  }
}
