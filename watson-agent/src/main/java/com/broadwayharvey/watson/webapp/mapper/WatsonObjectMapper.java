package com.broadwayharvey.watson.webapp.mapper;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.util.ISO8601DateFormat;

public class WatsonObjectMapper extends ObjectMapper {
  public WatsonObjectMapper() {
    configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
    setDateFormat(new ISO8601DateFormat());
  }
}
