package com.broadwayharvey.watson.webapp.bean;

import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;

import com.broadwayharvey.watson.model.SearchTag;
import com.broadwayharvey.watson.model.Verification;

@XmlRootElement
public class VerificationRequest extends BaseForm {
  /**
	 * 
	 */
  private static final long serialVersionUID = -2067160819195351751L;
  @Id
  protected BigInteger id;
  private String documentNumber;
  private String documentType;
  private String issuingCountry;
  private String referenceNumber;
  private List<SearchTag> tags;

  public BigInteger getId() {
    return id;
  }

  public List<SearchTag> getTags() {
    return tags;
  }

  public void setTags(List<SearchTag> tags) {
    this.tags = tags;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
  }

  public String getDocumentType() {
    return documentType;
  }

  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }

  public String getIssuingCountry() {
    return issuingCountry;
  }

  public void setIssuingCountry(String issuingCountry) {
    this.issuingCountry = issuingCountry;
  }

  public String getReferenceNumber() {
    return referenceNumber;
  }

  public void setReferenceNumber(String referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  @Override
  public String toString() {

    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("documentNumber", this.documentNumber).append("documentType", this.documentType)
        .append("issuingCountry", this.issuingCountry)
        .append("referenceNumber", this.referenceNumber).toString();
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Verification)) {
      return false;
    }
    VerificationRequest rhs = (VerificationRequest) o;
    return new EqualsBuilder().append(this.documentNumber, rhs.documentNumber)
        .append(this.documentType, rhs.documentType)
        .append(this.issuingCountry, rhs.issuingCountry)
        .append(this.referenceNumber, rhs.referenceNumber).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(1, 31).append(documentNumber).append(documentType)
        .append(issuingCountry).append(referenceNumber).toHashCode();
  }

}
