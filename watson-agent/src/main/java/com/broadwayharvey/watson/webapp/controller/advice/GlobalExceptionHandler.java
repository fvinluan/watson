package com.broadwayharvey.watson.webapp.controller.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.broadwayharvey.watson.exception.WatsonException;
import com.broadwayharvey.watson.webapp.bean.StatusMessage;

@ControllerAdvice
public class GlobalExceptionHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler(Exception.class)
  public ResponseEntity<String> handleWatsonException(Exception ex) {
    LOGGER.error("Exception " + ex.getMessage());
    return new ResponseEntity<String>("UNKNOWN ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(WatsonException.class)
  public ResponseEntity<StatusMessage> handleWatsonException(WatsonException ex) {
    LOGGER.info("Watson Exception " + ex.getMessage());
    StatusMessage e = new StatusMessage();
    e.setStatus(ex.getCode().getHttpStatus().value());
    e.setCode(ex.getCode().getCode());
    e.setMessage(ex.getMessage());

    LOGGER.info(e.toString());
    return new ResponseEntity<StatusMessage>(e, ex.getCode().getHttpStatus());
  }
}
