package com.broadwayharvey.watson.webapp.bean;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement
public class StatusMessage {
  private int code;
  private String developerMessage;
  private String message;
  private int status;
  private String uri;

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public StatusMessage() {}

  public int getCode() {
    return code;
  }

  public String getDeveloperMessage() {
    return developerMessage;
  }

  public String getMessage() {
    return message;
  }

  public int getStatus() {
    return status;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public void setDeveloperMessage(String developerMessage) {
    this.developerMessage = developerMessage;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("status", this.status).append("code", this.code)
        .append("developerMessage", this.developerMessage).append("message", this.message)
        .toString();
  }

}
