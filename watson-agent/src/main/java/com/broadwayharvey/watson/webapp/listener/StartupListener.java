package com.broadwayharvey.watson.webapp.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * StartupListener class used to initialize and database settings and populate any application-wide
 * drop-downs.
 * <p>
 * Keep in mind that this listener is executed outside of OpenSessionInViewFilter, so if you're
 * using Hibernate you'll have to explicitly initialize all loaded data at the DM or service level
 * to avoid LazyInitializationException. Hibernate.initialize() works well for doing this.
 * 
 * @author <a href="mailto:fvinluan@gmail.com">Francis Vinluan</a>
 */
public class StartupListener
    implements ServletContextListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(StartupListener.class);

  public void contextDestroyed(ServletContextEvent arg0) {

  }

  public void contextInitialized(ServletContextEvent arg0) {

    StringBuilder sb = new StringBuilder();
    sb.append(" __      __  _________________________________    _______   \n");
    sb.append(" /  \\    /  \\/  _  \\__    ___/   _____/\\_____  \\   \\      \\  \n");
    sb.append(" \\   \\/\\/   /  /_\\  \\|    |  \\_____  \\  /   |   \\  /   |   \\ \n");
    sb.append("  \\        /    |    \\    |  /        \\/    |    \\/    |    \\\n");
    sb.append("   \\__/\\  /\\____|__  /____| /_______  /\\_______  /\\____|__  /\n");
    sb.append("        \\/         \\/               \\/         \\/         \\/ \n");
    sb.append("WATSON AGENT IS OPEN FOR BUSINESS");
    LOGGER.info(sb.toString());

  }
}
