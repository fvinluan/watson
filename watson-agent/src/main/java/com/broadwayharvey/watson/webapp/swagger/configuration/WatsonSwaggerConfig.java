package com.broadwayharvey.watson.webapp.swagger.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;
import com.wordnik.swagger.model.ApiInfo;


@Configuration
@EnableSwagger
public class WatsonSwaggerConfig {
  private SpringSwaggerConfig springSwaggerConfig;

  /**
   * Required to autowire SpringSwaggerConfig
   */
  @Autowired
  public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig) {
    this.springSwaggerConfig = springSwaggerConfig;
  }

  /**
   * Every SwaggerSpringMvcPlugin bean is picked up by the swagger-mvc framework - allowing for
   * multiple swagger groups i.e. same code base multiple swagger resource listings.
   */
  @Bean
  public SwaggerSpringMvcPlugin customImplementation() {
    WatsonSwaggerPathProvider s = new WatsonSwaggerPathProvider();
    return new SwaggerSpringMvcPlugin(this.springSwaggerConfig).apiInfo(apiInfo())
        .swaggerGroup("service").pathProvider(s);
  }

  private ApiInfo apiInfo() {
    ApiInfo apiInfo =
        new ApiInfo(
            "WATSON API",
            "Mobile applications",
            "",
            "fvinluan@gmail.com",
            "2014, Harvey and Broadway LLC. All Rights Reserved. Site content may not be reproduced without express written permission",
            "");
    return apiInfo;
  }
}
