package com.broadwayharvey.watson.webapp.util;

import java.beans.PropertyDescriptor;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeanUtils;

import com.broadwayharvey.watson.model.BaseObject;
import com.broadwayharvey.watson.model.LabelValue;

/**
 * Utility class to convert one object to another.
 * 
 * 
 * 
 * 
 * 
 * 
 */
public final class ConvertUtil {
  // ~ Static fields/initializers
  // =============================================

  private static final Logger LOGGER = LoggerFactory.getLogger(ConvertUtil.class);

  // ~ Methods
  // ================================================================

  /**
   * Convenience method to convert a form to a POJO and back again
   * 
   * @param o the object to tranfer properties from
   * @return converted object
   */
  public static Object convert(Object o) throws Exception {
    if (o == null) {
      return null;
    }
    Object target = getOpposingObject(o);
    BeanUtils.copyProperties(target, o);
    return target;
  }

  /**
   * Method to convert a ResourceBundle to a Map object.
   * 
   * @param rb a given resource bundle
   * @return Map a populated map
   */
  public static Map convertBundleToMap(ResourceBundle rb) {
    Map map = new HashMap();

    for (Enumeration keys = rb.getKeys(); keys.hasMoreElements();) {
      String key = (String) keys.nextElement();
      map.put(key, rb.getString(key));
    }

    return map;
  }

  /**
   * Method to convert a ResourceBundle to a Properties object.
   * 
   * @param rb a given resource bundle
   * @return Properties a populated properties object
   */
  public static Properties convertBundleToProperties(ResourceBundle rb) {
    Properties props = new Properties();

    for (Enumeration keys = rb.getKeys(); keys.hasMoreElements();) {
      String key = (String) keys.nextElement();
      props.put(key, rb.getString(key));
    }

    return props;
  }

  /**
   * Convenience method to convert Lists (in a Form) from POJOs to Forms. Also checks for and
   * formats dates.
   * 
   * @param o
   * @return Object with converted lists
   * @throws Exception
   */
  public static Object convertLists(Object o) throws Exception {
    if (o == null) {
      return null;
    }

    Object target = null;

    PropertyDescriptor[] origDescriptors = PropertyUtils.getPropertyDescriptors(o);

    for (PropertyDescriptor origDescriptor : origDescriptors) {
      String name = origDescriptor.getName();

      if (origDescriptor.getPropertyType().equals(List.class)) {
        List list = (List) PropertyUtils.getProperty(o, name);
        for (int j = 0; j < list.size(); j++) {
          Object origin = list.get(j);
          target = convert(origin);
          list.set(j, target);
        }
        PropertyUtils.setProperty(o, name, list);
      }
    }
    return o;
  }

  public static Map convertListToMap(List list) {
    Map map = new LinkedHashMap();

    for (Iterator it = list.iterator(); it.hasNext();) {
      LabelValue option = (LabelValue) it.next();
      map.put(option.getLabel(), option.getValue());
    }

    return map;
  }

  /**
   * This method inspects a POJO or Form and figures out its pojo/form equivalent.
   * 
   * @param o the object to inspect
   * @return the Class of the persistable object
   * @throws ClassNotFoundException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  public static Object getOpposingObject(Object o) throws ClassNotFoundException,
      InstantiationException, IllegalAccessException {
    String name = o.getClass().getName();

    if (o instanceof BaseObject) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getting form equivalent of pojo...");
      }

      name = StringUtils.replace(name, ".model.", ".webapp.form.");
      if (AopUtils.isCglibProxy(o)) {
        name = name.substring(0, name.indexOf("$$"));
      }
      name += "Form";
    } else {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("getting pojo equivalent of form...");
      }
      name = StringUtils.replace(name, ".webapp.form.", ".model.");
      name = name.substring(0, name.lastIndexOf("Form"));
    }

    Class obj = Class.forName(name);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("returning className: " + obj.getName());
    }

    return obj.newInstance();
  }

  /**
   * Convenience method used by tests to populate an object from a ResourceBundle
   * 
   * @param obj an initialized object
   * @param rb a resource bundle
   * @return a populated object
   */
  public static Object populateObject(Object obj, ResourceBundle rb) {
    try {
      Map map = convertBundleToMap(rb);

      BeanUtils.copyProperties(obj, map);
    } catch (Exception e) {
      LOGGER.error("Exception occured populating object: " + e.getMessage());
    }

    return obj;
  }
}
