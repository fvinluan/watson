package com.broadwayharvey.watson.webapp.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionListener
    implements HttpSessionListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(SessionListener.class);

  private int sessionCount;

  public SessionListener() {
    this.sessionCount = 0;
  }

  public void sessionCreated(HttpSessionEvent se) {
    LOGGER.info("Created " + se.getSession().getId() + " session");
    synchronized (this) {
      sessionCount++;
      LOGGER.info("Active Session count : {}", sessionCount);
    }
  }

  public void sessionDestroyed(HttpSessionEvent se) {
    LOGGER.info("Destroyed " + se.getSession().getId() + " session");
    synchronized (this) {
      --sessionCount;
      LOGGER.info("Active Session count : {}", sessionCount);
    }
  }
}
