package com.broadwayharvey.watson.webapp.swagger.configuration;

import com.mangofactory.swagger.paths.SwaggerPathProvider;


public class WatsonSwaggerPathProvider
    extends SwaggerPathProvider {

  @Override
  protected String applicationPath() {
    return "/watsonagent/service";
  }

  @Override
  protected String getDocumentationPath() {
    return "/";
  }
}
