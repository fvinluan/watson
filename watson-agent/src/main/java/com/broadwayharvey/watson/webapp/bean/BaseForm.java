package com.broadwayharvey.watson.webapp.bean;

import java.io.Serializable;

public abstract class BaseForm
    implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 8268220447361010064L;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public abstract boolean equals(Object o);

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public abstract String toString();

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public abstract int hashCode();


}
