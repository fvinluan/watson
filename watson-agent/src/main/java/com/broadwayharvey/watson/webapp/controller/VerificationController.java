package com.broadwayharvey.watson.webapp.controller;

import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.broadwayharvey.watson.model.Verification;
import com.broadwayharvey.watson.service.VerificationService;
import com.broadwayharvey.watson.webapp.bean.StatusMessage;
import com.broadwayharvey.watson.webapp.bean.VerificationRequest;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@Api(value = "Verifications", description = "Verifications")
@Produces({"application/json", "application/xml"})
public class VerificationController extends BaseController {

  private static final Logger LOGGER = LoggerFactory.getLogger(VerificationController.class);

  @Autowired
  VerificationService verificationService;

  @RequestMapping(method = RequestMethod.POST, value = "/verifications")
  @ResponseStatus(HttpStatus.ACCEPTED)
  public @ResponseBody StatusMessage addVerification(@ApiParam(name = "body", value = "verification request",
      required = true) 
      @RequestBody VerificationRequest verificationRequest) throws Exception {

    LOGGER.info("Submitted " + verificationRequest.toString());

    Verification req =
        new Verification(verificationRequest.getDocumentNumber(),
            verificationRequest.getDocumentType(), verificationRequest.getIssuingCountry(),
            verificationRequest.getReferenceNumber());
    req.setTags(verificationRequest.getTags());
    verificationService.postVerificationRequest(req);

    StatusMessage sm = new StatusMessage();
    sm.setStatus(HttpStatus.ACCEPTED.value());
    sm.setMessage("Accepted " + verificationRequest.getId());
    return sm;
  }

  public void setVerificationService(VerificationService verificationService) {
    this.verificationService = verificationService;
  }

}
