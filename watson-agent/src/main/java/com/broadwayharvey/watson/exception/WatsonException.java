package com.broadwayharvey.watson.exception;

import com.broadwayharvey.watson.ExceptionCode;

/**
 * 
 * @author <a href=mailto:fvinluan@gmail.com>Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
public class WatsonException extends RuntimeException {

  /**
     * 
     */
  private static final long serialVersionUID = 8621216058281380111L;
  private ExceptionCode code;
  private String message;

  /**
   * 
   * @param code
   */
  public WatsonException(ExceptionCode code) {
    super(code.getDescription());
    this.code = code;
  }

  /**
   * 
   * @param code
   */
  public WatsonException(ExceptionCode code, String message) {
    super(code.getDescription());
    this.code = code;
    this.message = message;
  }

  /**
   * 
   * @param code
   * @param cause
   */
  public WatsonException(ExceptionCode code, Throwable cause) {
    super(code.getDescription(), cause);
    this.code = code;
  }

  /**
   * 
   * @param cause
   */
  public WatsonException(Throwable cause) {
    super(cause);
  }

  public ExceptionCode getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

}
