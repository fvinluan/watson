package com.broadwayharvey.watson.rabbitmq.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.broadwayharvey.watson.rabbitmq.service.MessageProducerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 */
public class MessageProducerServiceImpl
    implements MessageProducerService {

  private String bindingKey;

  @Autowired
  private RabbitTemplate rabbitTemplate;
  private String routingKey;

  /**
   * Method buildMessageProperties.
   * 
   * @return MessageProperties
   */
  private MessageProperties buildMessageProperties() {
    MessageProperties props = new MessageProperties();
    props.setHeader("bindingKey", bindingKey);
    String messageId = UUID.randomUUID().toString();
    props.setMessageId(messageId);
    return props;
  }

  /**
   * Method getBindingKey.
   * 
   * @return String
   */
  public String getBindingKey() {
    return bindingKey;
  }

  /**
   * Method getRoutingKey.
   * 
   * @return String
   */
  public String getRoutingKey() {
    return routingKey;
  }

  /**
  * 
  */
  @Override
  public void publishToExchange(Object objectToSend) throws UnsupportedEncodingException,
      JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    String messageToSend = mapper.writeValueAsString(objectToSend);
    MessageProperties props = buildMessageProperties();
    Message message = new Message(messageToSend.getBytes("UTF-8"), props);
    // FIXME : set reply time out in the event that RabbitMQ is down
    rabbitTemplate.convertAndSend(routingKey, message);
  }

  /**
   * Method setBindingKey.
   * 
   * @param bindingKey String
   */
  public void setBindingKey(String bindingKey) {
    this.bindingKey = bindingKey;
  }

  /**
   * Method setRoutingKey.
   * 
   * @param routingKey String
   */
  public void setRoutingKey(String routingKey) {
    this.routingKey = routingKey;
  }

}
