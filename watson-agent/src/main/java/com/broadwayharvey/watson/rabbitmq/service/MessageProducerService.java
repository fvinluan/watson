package com.broadwayharvey.watson.rabbitmq.service;

import java.io.IOException;

/**
 */
public interface MessageProducerService {

  /**
   * Method publishToExchange.
   * @param objectToSend Object
   * @throws IOException
   */
  void publishToExchange(Object objectToSend) throws IOException;

}
