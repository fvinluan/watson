<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico"/>
        <title>PAGE NOT FOUND</title>
    </head>
    <body>
        <img src="<%=request.getContextPath()%>/images/404.jpg" alt="" style="position: absolute; left: 50%; top: 50%; margin-left: -285px; margin-top: -190px;">
    </body>
</html>