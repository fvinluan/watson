package com.broadwayharvey.watson.model;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A simple JavaBean to represent label-value pairs. This is most commonly used when constructing
 * user interface elements which have a label to be displayed to the user, and a corresponding value
 * to be returned to the server. One example is the <code>&lt;html:options&gt;</code> tag.
 * <p/>
 * <p/>
 * Note: this class has a natural ordering that is inconsistent with equals.
 * 
 * @author <a href="mailto:fvinluan@gmail.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@SuppressWarnings("unchecked")
public class LabelValue
    implements Comparable, Serializable {
  /**
   * Comparator that can be used for a case insensitive sort of <code>LabelValue</code> objects.
   */
  @SuppressWarnings("rawtypes")
  public static final Comparator CASE_INSENSITIVE_ORDER = new Comparator() {
    public int compare(Object o1, Object o2) {
      String label1 = ((LabelValue) o1).getLabel();
      String label2 = ((LabelValue) o2).getLabel();
      return label1.compareToIgnoreCase(label2);
    }
  };

  private static final long serialVersionUID = 3689355407466181430L;

  // ----------------------------------------------------------- Constructors

  private String field;

  /**
   * The property which supplies the option label visible to the end user.
   */
  private String label = null;

  private String model;

  /**
   * The property which supplies the value returned to the server.
   */
  private String value = null;

  // ------------------------------------------------------------- Properties

  /**
   * Default constructor.
   */
  public LabelValue() {
    super();
  }

  /**
   * Construct an instance with the supplied property values.
   * 
   * @param label The label to be displayed to the user.
   * @param value The value to be returned to the server.
   */
  public LabelValue(String label, String value) {
    this.label = label;
    this.value = value;
  }

  /**
   * Instantiates a new label value.
   * 
   * @param label the label
   * @param value the value
   * @param field the field
   * @param model the model
   */
  public LabelValue(String label, String value, String field, String model) {
    super();
    this.label = label;
    this.value = value;
    this.field = field;
    this.model = model;
  }

  /**
   * Compare LabelValueBeans based on the label, because that's the human viewable part of the
   * object.
   * 
   * @param o the o
   * 
   * 
   * @return the int * @see Comparable
   */
  public int compareTo(Object o) {
    // Implicitly tests for the correct type, throwing
    // ClassCastException as required by interface
    String otherLabel = ((LabelValue) o).getLabel();

    return this.getLabel().compareTo(otherLabel);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object object) {
    if (!(object instanceof LabelValue)) {
      return false;
    }

    LabelValue rhs = (LabelValue) object;
    return new EqualsBuilder().append(this.value, rhs.value).isEquals();
  }

  /**
   * Gets the field.
   * 
   * 
   * @return the field
   */
  public String getField() {
    return field;
  }

  /**
   * Gets the label.
   * 
   * 
   * @return the label
   */
  public String getLabel() {
    return this.label;
  }

  /**
   * Gets the model.
   * 
   * 
   * @return the model
   */
  public String getModel() {
    return model;
  }

  /**
   * Gets the value.
   * 
   * 
   * @return the value
   */
  public String getValue() {
    return this.value;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    // pick 2 hard-coded, odd, >0 ints as args
    return new HashCodeBuilder(1, 31).append(value).toHashCode();
  }

  /**
   * Sets the field.
   * 
   * @param field the field to set
   */
  public void setField(String field) {
    this.field = field;
  }

  /**
   * Sets the label.
   * 
   * @param label the new label
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * Sets the model.
   * 
   * @param model the model to set
   */
  public void setModel(String model) {
    this.model = model;
  }

  /**
   * Sets the value.
   * 
   * @param value the new value
   */
  public void setValue(String value) {
    this.value = value;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("LabelValue", this.label).append("value", this.value).toString();
  }

}
