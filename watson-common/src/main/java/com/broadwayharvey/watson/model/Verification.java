package com.broadwayharvey.watson.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "verifications")
@TypeAlias("verification")
public class Verification
    implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 4001728901878651540L;

  @Id
  protected BigInteger id;

  private String documentNumber;
  private String documentType;
  private String issuingCountry;
  private String referenceNumber;
  private List<SearchTag> tags;
  private Date searchDate;
  private int resultCode;
  private boolean positive;
  private String errorText;
  private String positiveText;
  private String description;
  private int restrictionType;

  public Verification() {

  }

  public Verification(String documentNumber, String documentType, String issuingCountry,
      String referenceNumber) {
    super();
    this.documentNumber = documentNumber;
    this.documentType = documentType;
    this.issuingCountry = issuingCountry;
    this.referenceNumber = referenceNumber;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Verification)) {
      return false;
    }
    Verification rhs = (Verification) o;
    return new EqualsBuilder().append(this.id, rhs.id).isEquals();
  }

  public String getDescription() {
    return description;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public String getDocumentType() {
    return documentType;
  }

  public String getErrorText() {
    return errorText;
  }

  public BigInteger getId() {
    return id;
  }

  public String getIssuingCountry() {
    return issuingCountry;
  }

  public String getPositiveText() {
    return positiveText;
  }

  public String getReferenceNumber() {
    return referenceNumber;
  }

  public int getRestrictionType() {
    return restrictionType;
  }

  public int getResultCode() {
    return resultCode;
  }

  public Date getSearchDate() {
    return searchDate;
  }

  public List<SearchTag> getTags() {
    return tags;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(1, 31).append(id).toHashCode();

  }

  public boolean isPositive() {
    return positive;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
  }

  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }

  public void setErrorText(String errorText) {
    this.errorText = errorText;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public void setIssuingCountry(String issuingCountry) {
    this.issuingCountry = issuingCountry;
  }

  public void setPositive(boolean positive) {
    this.positive = positive;
  }

  public void setPositiveText(String positiveText) {
    this.positiveText = positiveText;
  }

  public void setReferenceNumber(String referenceNumber) {
    this.referenceNumber = referenceNumber;
  }

  public void setRestrictionType(int restrictionType) {
    this.restrictionType = restrictionType;
  }

  public void setResultCode(int resultCode) {
    this.resultCode = resultCode;
  }

  public void setSearchDate(Date searchDate) {
    this.searchDate = searchDate;
  }

  public void setTags(List<SearchTag> tags) {
    this.tags = tags;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", this.id)
        .append("documentNumber", this.documentNumber).append("documentType", this.documentType)
        .toString();
  }

}
