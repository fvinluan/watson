package com.broadwayharvey.watson.model;

import java.io.Serializable;
import java.math.BigInteger;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
@TypeAlias("user")
public class User
    implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 4001728901878651540L;

  public User(String username, String password) {
    super();
    this.username = username;
    this.password = password;
  }

  @Id
  protected BigInteger id;


  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  String username;

  String password;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof User)) {
      return false;
    }
    User rhs = (User) o;
    return new EqualsBuilder().append(this.id, rhs.id).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(1, 31).append(id).toHashCode();

  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", this.id)
        .toString();
  }


}
