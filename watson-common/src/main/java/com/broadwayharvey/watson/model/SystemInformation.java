package com.broadwayharvey.watson.model;

import java.io.Serializable;
import java.math.BigInteger;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "systemInformation")
@TypeAlias("systemInformation")
public class SystemInformation
    implements Serializable {


  /**
   * 
   */
  private static final long serialVersionUID = 6491762204107724290L;

  @Id
  protected BigInteger id;


  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  String verificationAPIURI;

  String verificationAPIUsername;

  String verificationAPIPassword;

  public String getVerificationAPIURI() {
    return verificationAPIURI;
  }

  public void setVerificationAPIURI(String verificationAPIURI) {
    this.verificationAPIURI = verificationAPIURI;
  }

  public String getVerificationAPIUsername() {
    return verificationAPIUsername;
  }

  public void setVerificationAPIUsername(String verificationAPIUsername) {
    this.verificationAPIUsername = verificationAPIUsername;
  }

  public String getVerificationAPIPassword() {
    return verificationAPIPassword;
  }

  public void setVerificationAPIPassword(String verificationAPIPassword) {
    this.verificationAPIPassword = verificationAPIPassword;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof SystemInformation)) {
      return false;
    }
    SystemInformation rhs = (SystemInformation) o;
    return new EqualsBuilder().append(this.id, rhs.id).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(1, 31).append(id).toHashCode();

  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", this.id)
        .toString();
  }


}
