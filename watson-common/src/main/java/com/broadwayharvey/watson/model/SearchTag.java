package com.broadwayharvey.watson.model;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A simple JavaBean to represent label-value pairs. This is most commonly used when constructing
 * user interface elements which have a label to be displayed to the user, and a corresponding value
 * to be returned to the server. One example is the <code>&lt;html:options&gt;</code> tag.
 * <p/>
 * <p/>
 * Note: this class has a natural ordering that is inconsistent with equals.
 * 
 * @author <a href="mailto:fvinluan@gmail.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@SuppressWarnings("unchecked")
public class SearchTag
    implements Comparable, Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -5282136054875523690L;


  /**
   * Comparator that can be used for a case insensitive sort of <code>LabelValue</code> objects.
   */
  @SuppressWarnings("rawtypes")
  public static final Comparator CASE_INSENSITIVE_ORDER = new Comparator() {
    public int compare(Object o1, Object o2) {
      String label1 = ((SearchTag) o1).getTag();
      String label2 = ((SearchTag) o2).getTag();
      return label1.compareToIgnoreCase(label2);
    }
  };


  private String tag;


  private String value = null;

  public SearchTag() {
    super();
  }

  /**
   * Construct an instance with the supplied property values.
   * 
   * @param label The label to be displayed to the user.
   * @param value The value to be returned to the server.
   */
  public SearchTag(String tag, String value) {
    this.tag = tag;
    this.value = value;
  }


  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  /**
   * Compare LabelValueBeans based on the label, because that's the human viewable part of the
   * object.
   * 
   * @param o the o
   * 
   * 
   * @return the int * @see Comparable
   */
  public int compareTo(Object o) {
    // Implicitly tests for the correct type, throwing
    // ClassCastException as required by interface
    String otherLabel = ((SearchTag) o).getTag();

    return this.getTag().compareTo(otherLabel);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object object) {
    if (!(object instanceof SearchTag)) {
      return false;
    }

    SearchTag rhs = (SearchTag) object;
    return new EqualsBuilder().append(this.value, rhs.value).isEquals();
  }



  /**
   * Gets the value.
   * 
   * 
   * @return the value
   */
  public String getValue() {
    return this.value;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    // pick 2 hard-coded, odd, >0 ints as args
    return new HashCodeBuilder(1, 31).append(value).toHashCode();
  }

  /**
   * Sets the value.
   * 
   * @param value the new value
   */
  public void setValue(String value) {
    this.value = value;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("tag", this.tag)
        .append("value", this.value).toString();
  }

}
